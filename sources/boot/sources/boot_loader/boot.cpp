#include <malloc.h>
#include <stdlib.h>
#include <string.h>

/* common include */
#include "ring_buffer.h"
#include "xprintf.h"

/* driver include */
#include "led.h"
#include "button.h"

/* app include */
#include "boot.h"
#include "boot_dbg.h"

#include "platform.h"
#include "io_cfg.h"

/* sys include */
#include "sys_ctrl.h"
#include "sys_dbg.h"


using namespace std;

/**
 * @brief Extern and global variables
 **/
led_t LED_Life;


int main_boot() {
    /************************************************************
    * Init applications                                         *
    *************************************************************/
    ENTRY_CRITICAL();

    /************************
    * Software configure    *
    *************************/

    /***********************
    * Hardware configure   *
    ************************/
    /* LED life init */
    led_init(&LED_Life, LEDLifeConfig, LEDLifeOn, LEDLifeOff);
    LEDsUserConfig();

    EXIT_CRITICAL();

    /**
     * @brief Jump to app program address
     * if no happen occurs
     **/
    SystemControl_JumpToAppReq = SYS_CTRL_JUMP_TO_APP_REQ;
    SystemBOOTControlReset();
    
    /****************************************************
    * Run applications                                  *
    *****************************************************/
    for (;;) {
        led_toggle(&LED_Life);
        DELAY_TICK_Ms(100);
    }

    return 0;
}

