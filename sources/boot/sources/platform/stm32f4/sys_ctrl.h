#ifndef __SYS_CTRL_H__
#define __SYS_CTRL_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "startup_code.h"

/*********************************************
 * @define: SYSTEM CONTROL GENEREAL DEFINES  *
 *********************************************/
#define SYS_CTRL_JUMP_TO_APP_REQ	                ((uint32_t)0xEFEFEFEF)


/**
 * @brief Extern variables
 **/
 extern uint32_t SystemControl_JumpToAppReq;


/**
 * @brief System control 
 * function prototypes
 **/
extern void SystemBOOTControlReset(void);
extern void SystemBOOTControlJumpToApp(void);
extern void SystemBOOTControlShellPutChar(uint8_t);

extern void DELAY_TICK_Ms(uint32_t time_out_ms);
extern void DELAY_TICK_Us(uint32_t time_out_us);

extern uint32_t MILLIS(void);
extern uint32_t MICROS(void);



#ifdef __cplusplus
}
#endif

#endif // __SYS_CTRL_H__
