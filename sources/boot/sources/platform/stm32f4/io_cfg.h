#ifndef __IO_CFG_H__
#define __IO_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "system_stm32f4xx.h"


/*********************************
 * @define: LED PIN MAP DEFINES  *
 *********************************/
#define USER_LED_CLOCK_GATE			( RCC_AHB1Periph_GPIOD )
#define LED_LIFE    				( GPIO_Pin_12 )
#define USER_ORANGE_LED				( GPIO_Pin_13 )
#define USER_RED_LED				( GPIO_Pin_14 )
#define USER_BLUE_LED				( GPIO_Pin_15 )
#define USER_LED_MASK				( USER_ORANGE_LED | USER_RED_LED | USER_BLUE_LED )
#define USER_LED_PORT				( GPIOD )

/**
 * @brief LED control 
 * function prototypes
 **/
extern void LEDLifeConfig(void);
extern void LEDLifeOn(void);
extern void LEDLifeOff(void);

extern void LEDsUserConfig(void);
extern void LEDsUserOn(uint16_t user_led);
extern void LEDsUserOff(uint16_t user_led);
extern void LEDsUserToggle(uint16_t user_led);


#ifdef __cplusplus
}
#endif

#endif //__IO_CFG_H__
