#include <stdint.h>
#include <stdbool.h>

#include "io_cfg.h"
#include "platform.h"

#include "sys_dbg.h"
#include "sys_ctrl.h"

#include "startup_code.h"


/********************************************************
 * 														*
 * 														*
 * 														*
 * 						LED FUNTIONS					*
 * 														*
 *  													*
 *  													*
 ********************************************************/
void LEDLifeConfig() {
    GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(USER_LED_CLOCK_GATE, ENABLE);

	GPIO_InitStructure.GPIO_Pin = LED_LIFE;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(USER_LED_PORT, &GPIO_InitStructure);
}

void LEDLifeOn() {
    GPIO_ResetBits(USER_LED_PORT, LED_LIFE);
}

void LEDLifeOff() {
    GPIO_SetBits(USER_LED_PORT, LED_LIFE);
}

void LEDsUserConfig() {
    GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(USER_LED_CLOCK_GATE, ENABLE);

	GPIO_InitStructure.GPIO_Pin = USER_LED_MASK;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(USER_LED_PORT, &GPIO_InitStructure);
}

void LEDsUserOn(uint16_t user_led) {
    GPIO_ResetBits(USER_LED_PORT, user_led);
}

void LEDsUserOff(uint16_t user_led) {
    GPIO_SetBits(USER_LED_PORT, user_led);
}

void LEDsUserToggle(uint16_t user_led) {
    GPIO_ToggleBits(USER_LED_PORT, user_led);
}
