#ifndef __SYS_CFG_H__
#define __SYS_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "ring_buffer.h"

/*******************************************
 * @define: SYSTEM SHELL GENEREAL DEFINES  *
 *******************************************/
#define SYS_SHELL_UART_CLOCK                ( RCC_APB1Periph_USART2 )
#define SYS_SHELL_IRQn                      ( USART2_IRQn )
#define SYS_SHELL_UART                      ( USART2 )

#define SYS_SHELL_IO_PORT_CLOCK             ( RCC_AHB1Periph_GPIOD )
#define SYS_SHELL_IO_PORT                   ( GPIOD )
#define SYS_SHELL_IO_AF                     ( GPIO_AF_USART2 )

#define SYS_SHELL_TX_PIN                    ( GPIO_Pin_5 )
#define SYS_SHELL_TX_PIN_SOURCE             ( GPIO_PinSource5 )

#define SYS_SHELL_RX_PIN                    ( GPIO_Pin_6 )
#define SYS_SHELL_RX_PIN_SOURCE             ( GPIO_PinSource6 )

#define BUFFER_CHAR_SHELL_SEND_SIZE		    ( 512 )

/************************************
 * @brief EXTERN & GLOBAL VARIABLES *
 ************************************/
extern ring_buffer_char_t SysShellSendChar;
extern uint8_t BufferCharSend[];

/********************************
 * @brief: FUNCTION PROTOTYPES  *
 ********************************/
extern void SystemBOOTTickConfig(void);
extern void SystemBOOTShellConfig(void);
extern void SystemBOOTUpdate(void);


#ifdef __cplusplus
}
#endif

#endif //__SYS_CFG_H__
