#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#ifdef __cplusplus
extern "C"
{
#endif

/*************************************
 * @define: PLATFORM GENERAL DEFINES *
 *************************************/
#define ENTRY_CRITICAL()             EntryCritical()
#define EXIT_CRITICAL()              ExitCritical()
#define ENABLE_INTERRUPTS()          EnableIRQ()
#define DISABLE_INTERRUPTS()         DisableIRQ()

#define LOG2LKUP(val)              ((uint_fast8_t)(32U - __builtin_clz(val)))

/********************************
 * @brief: FUNCTION PROTOTYPES  *
 ********************************/
extern void EnableIRQ(void);
extern void DisableIRQ();
extern void EntryCritical();
extern void ExitCritical();
extern int  GetNestEntryCriticalCounter();

#ifdef __cplusplus
}
#endif

#endif /* __PLATFORM_H__ */