include sources/platform/stm32f4/Libraries/STM32F4xx_StdPeriph_Driver/Makefile.mk
include sources/platform/stm32f4/Libraries/CMSIS/Makefile.mk

CFLAGS 		+= -Isources/platform/stm32f4
CPPFLAGS 	+= -Isources/platform/stm32f4

# C source files
C_SOURCES += sources/platform/stm32f4/platform.c
C_SOURCES += sources/platform/stm32f4/startup_code.c
C_SOURCES += sources/platform/stm32f4/sys_cfg.c
C_SOURCES += sources/platform/stm32f4/io_cfg.c
C_SOURCES += sources/platform/stm32f4/stm32f4xx_it.c
C_SOURCES += sources/platform/stm32f4/system_stm32f4xx.c

