#ifndef __SYS_DBG_H__
#define __SYS_DBG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include "io_cfg.h"
#include "sys_ctrl.h"
#include "platform.h"
#include "xprintf.h"

/*******************************************
 * @define: SYSTEM DEBUG GENEREAL DEFINES  *
 *******************************************/
#if defined(SYS_DBG_EN)
#define SYS_DBG(fmt, ...)         xprintf((const char*)fmt, ##__VA_ARGS__)
#else
#define SYS_DBG(fmt, ...)
#endif

#if defined(SYS_PRINT_EN)
#define SYS_PRINT(fmt, ...)       xprintf((const char*)fmt, ##__VA_ARGS__)
#else
#define SYS_PRINT(fmt, ...)
#endif

#define FATAL(s, c)                                 \
    do {                                            \
        DisableIRQ();                               \
        SYS_PRINT((const int8_t*)s, (uint8_t)c);    \
        while(1) {                                  \
            LEDLifeOn();                            \
            DELAY_TICK_Us(10000);                   \
            LEDLifeOff();                           \
            DELAY_TICK_Us(10000);                   \
        }                                           \
    } while (0)

#ifdef __cplusplus
}
#endif

#endif //__SYS_DBG_H__