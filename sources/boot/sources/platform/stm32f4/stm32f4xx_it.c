#include <stdint.h>
#include <stdbool.h>

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#include "xprintf.h"
#include "ring_buffer.h"

#include "sys_dbg.h"
#include "sys_cfg.h"
#include "sys_ctrl.h"
#include "sys_dbg.h"


/************************************
 * @brief EXTERN & GLOBAL VARIABLES *
 ************************************/
extern ring_buffer_char_t ring_buffer_console_rev;
extern ring_buffer_char_t SysShellSendChar;

/****************************
 * @brief PRIVATE VARIABLES *
 ****************************/
static volatile uint32_t s_MsTick = 0;
static volatile uint32_t s_UsTick = 0;


/**********************************************
 * @brief CORTEX-M PROCESSOR FAULT-EXCEPTIONS *
 **********************************************/
void NMI_IRQHandler() {
	FATAL("SY", 0x01);
}
void HardFault_IRQHandler() {
	FATAL("SY", 0x02);
}
void MemManageFault_IRQHandler() {
	FATAL("SY", 0x03);
}
void BusFault_IRQHandler() {
	FATAL("SY", 0x04);
}
void UsageFault_IRQHandler() {
	FATAL("SY", 0x05);
}

/**************************************************
 * @brief CORTEX-M PROCESSOR NON-FAULT-EXCEPTIONS *
 **************************************************/
void SysTick_IRQHandler() {
	/**
	 * Increase MILLIS seconds 
	 **/
	++(s_MsTick);
}

uint32_t MILLIS() {
	return s_MsTick;
}

uint32_t MICROS() {
	uint32_t m = s_MsTick;
	const uint32_t tms = SysTick->LOAD + 1;
	__IO uint32_t u = tms - SysTick->VAL;

	/* Checks if the Systick counter flag is active or not */
	if (((SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) == (SysTick_CTRL_COUNTFLAG_Msk))) {
		m = s_MsTick;
		u = tms - SysTick->VAL;
	}

	return (m * 1000 + (u * 1000) / tms);
}

