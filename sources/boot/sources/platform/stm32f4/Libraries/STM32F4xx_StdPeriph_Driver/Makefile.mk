CFLAGS += -Isources/platform/stm32f4/Libraries/STM32F4xx_StdPeriph_Driver/inc
CPPFLAGS += -Isources/platform/stm32f4/Libraries/STM32F4xx_StdPeriph_Driver/inc

STD_LIB_SOURCES_PATH = sources/platform/stm32f4/Libraries/STM32F4xx_StdPeriph_Driver/src

# C source files
C_SOURCES += $(STD_LIB_SOURCES_PATH)/misc.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_gpio.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_rcc.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_usart.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_spi.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_exti.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_syscfg.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_tim.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_adc.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_crc.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_rtc.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_pwr.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_flash.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_iwdg.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_dac.c
C_SOURCES += $(STD_LIB_SOURCES_PATH)/stm32f4xx_i2c.c
