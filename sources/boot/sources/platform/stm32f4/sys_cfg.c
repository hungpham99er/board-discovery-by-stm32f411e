#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>

#include "sys_cfg.h"
#include "startup_code.h"

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "system_stm32f4xx.h"

#include "xprintf.h"
#include "ring_buffer.h"

#include "boot.h"

#include "sys_dbg.h"
#include "sys_ctrl.h"
#include "sys_dbg.h"


/************************************
 * @brief EXTERN & GLOBAL VARIABLES *
 ************************************/
ring_buffer_char_t SysShellSendChar;
uint8_t BufferCharSend[BUFFER_CHAR_SHELL_SEND_SIZE];

uint32_t SystemControl_JumpToAppReq;

/********************************************************
 * 														*
 * 														*
 * 														*
 * 					SYSTEM CONFIGURATIONS				*
 * 														*
 *  													*
 *  													*
 ********************************************************/
void SystemBOOTTickConfig() {
    SysTick_Config(SystemCoreClock / 1000);
}

void SystemBOOTShellConfig() {
    GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	RCC_APB1PeriphClockCmd(SYS_SHELL_UART_CLOCK, ENABLE);
	RCC_AHB1PeriphClockCmd(SYS_SHELL_IO_PORT_CLOCK, ENABLE);

	GPIO_PinAFConfig(SYS_SHELL_IO_PORT, SYS_SHELL_TX_PIN_SOURCE, SYS_SHELL_IO_AF);
	GPIO_PinAFConfig(SYS_SHELL_IO_PORT, SYS_SHELL_RX_PIN_SOURCE, SYS_SHELL_IO_AF);

	GPIO_InitStructure.GPIO_Pin = SYS_SHELL_TX_PIN | SYS_SHELL_RX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(SYS_SHELL_IO_PORT, &GPIO_InitStructure);

	USART_InitStructure.USART_BaudRate = 460800;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(SYS_SHELL_UART, &USART_InitStructure);

	USART_Cmd(SYS_SHELL_UART, ENABLE);

	ring_buffer_char_init(&SysShellSendChar, BufferCharSend, BUFFER_CHAR_SHELL_SEND_SIZE);
    xfunc_output = (void(*))SystemBOOTControlShellPutChar;
}

void SystemBOOTUpdate() {
	uint8_t i, j;

	SYS_PRINT("\n");
	SYS_PRINT(" _____\n");
	SYS_PRINT("(  _  \\                  _\n");
	SYS_PRINT("( (_) )  _____  _____  _( )_\n");
	SYS_PRINT("(  _  \\ (  _  )(  _  )(_   _)\n");
	SYS_PRINT("( (_) ) | (_) || (_) |  ( )\n");
	SYS_PRINT("(_____/ (_____)(_____)  (_)\n");

	SYS_PRINT("\n");
    
}

/********************************************************
 * 														*
 * 														*
 * 														*
 * 					SYSTEM CONTROL						*
 * 														*
 *  													*
 *  													*
 ********************************************************/
void SystemBOOTControlReset() {
    NVIC_SystemReset();
}

void DELAY_TICK_Ms(uint32_t time_out_ms) {
    uint32_t now = MILLIS();
    while (MILLIS() - now < time_out_ms) {
        /* YIELD() */
    }
}

void DELAY_TICK_Us(uint32_t time_out_us) {
    __IO uint32_t currentTicks = SysTick->VAL;
	/* Number of ticks per MILLISecond */
	const uint32_t tickPerMs = SysTick->LOAD + 1;
	/* Number of ticks to count */
	const uint32_t nbTicks = ((time_out_us - ((time_out_us > 0) ? 1 : 0)) * tickPerMs) / 1000;
	/* Number of elapsed ticks */
	uint32_t elapsedTicks = 0;
	__IO uint32_t oldTicks = currentTicks;
	do {
		currentTicks = SysTick->VAL;
		elapsedTicks += (oldTicks < currentTicks) ? tickPerMs + oldTicks - currentTicks :
													oldTicks - currentTicks;
		oldTicks = currentTicks;
	} while (nbTicks > elapsedTicks);
}

void SystemBOOTControlShellPutChar(uint8_t c) {
	while (USART_GetFlagStatus(SYS_SHELL_UART, USART_FLAG_TXE) == RESET);
	USART_SendData(SYS_SHELL_UART, c);
	while (USART_GetFlagStatus(SYS_SHELL_UART, USART_FLAG_TC) == RESET);
}

/********************************************************
 * 														*
 * 														*
 * 														*
 * 					SYSTEM JUMP TO....					*
 * 														*
 *  													*
 *  													*
 ********************************************************/
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated"

void SystemBOOTControlJumpToApp() {
	volatile uint32_t AppProgramAddress = (uint32_t) *(volatile uint32_t*)(NORMAL_START_ADDRESS + 4);
	pf_JumpFunction JumpToAppAddr = (pf_JumpFunction)AppProgramAddress;

	/* Disable interrupt */
	__disable_irq();

	/* Set stack pointer */
	__DMB();
    __set_MSP((uint32_t) *(volatile uint32_t*)(NORMAL_START_ADDRESS));
	__DSB();

	/* Jump to app program */
	JumpToAppAddr();
}
#pragma GCC diagnostic pop




