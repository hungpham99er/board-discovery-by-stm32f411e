CFLAGS		+= -Isources/driver/led
CPPFLAGS	+= -Isources/driver/led

CFLAGS		+= -Isources/driver/button
CPPFLAGS	+= -Isources/driver/button

DRIVER_SOURCE_PATH = sources/driver

# LED
C_SOURCES += $(DRIVER_SOURCE_PATH)/led/led.c

# Button
C_SOURCES += $(DRIVER_SOURCE_PATH)/button/button.c

