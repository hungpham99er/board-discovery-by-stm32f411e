CFLAGS		+= -Isources/common
CPPFLAGS	+= -Isources/common

CFLAGS		+= -Isources/common/container
CPPFLAGS	+= -Isources/common/container

COMMON_SOURCE_PATH = sources/common
C_SOURCES += $(COMMON_SOURCE_PATH)/xprintf.c
C_SOURCES += $(COMMON_SOURCE_PATH)/cmd_line.c
C_SOURCES += $(COMMON_SOURCE_PATH)/utils.c

CONTAINER_SOURCE_PATH = sources/common/container
C_SOURCES += $(CONTAINER_SOURCE_PATH)/log_queue.c
C_SOURCES += $(CONTAINER_SOURCE_PATH)/ring_buffer.c


