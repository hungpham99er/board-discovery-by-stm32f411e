/**
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
 * @author HungPNQ                                  @
 * @brief Porting Dynamic Allocation                @
 * @date 29/03/2022                                 @
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 */


#ifndef __HEAP_H__
#define __HEAP_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <stddef.h>

#include "port.h"
#include "sys_dbg.h"


/**********************
 *    LINKER SCRIPT   *
 **********************/
extern uint32_t __heap_start__;
extern uint32_t __heap_end__;

/**********************************
 * @define: HEAP GENERAL DEFINES  *
 **********************************/
#define TOTAL_HEAP_SIZE                 ( 3072U )

#define HEAP_START_ADDR                 ( &__heap_start__ )
#define HEAP_END_ADDR                   ( &__heap_end__   )

#define BLOCK_LINK_STRUCT_SIZE          ( sizeof(struct BLOCK_LINK) )

/* FATAL heap */
#define INVALID_VALUE_ALLOCATED()       FATAL("HEAP", 0x01)
#define INSUFFICENT_HEAP_MEMORY()       FATAL("HEAP", 0x02)
#define HEAP_OVERFLOW()                 FATAL("HEAP", 0x03)
#define INVALID_VALUE_FREE()            FATAL("HEAP", 0x04)
#define INVALID_BLOCK_TO_FREE()         FATAL("HEAP", 0x05)

typedef struct HEAP_REGION {
    uint8_t  *pxHeap_Start_Address;
    uint32_t Total_Size;
    uint32_t Free_Size;
    uint32_t Used_Size;
} HeapRegion_t;

typedef struct BLOCK_LINK {
    struct BLOCK_LINK *pNext_FreeBLOCK;
    uint32_t SizeOfBLOCK;     
} BlockLink_t;

/********************************
 * @brief: FUNCTION PROTOTYPES  *
 ********************************/
extern void*    PortMalloc(uint32_t Num_Of_Byte);
extern void     PortFree(void *pxFree);
extern uint32_t Get_TotalHeapSize(void);
extern uint32_t Get_TotalHeapFree(void);
extern uint32_t Get_TotalHeapUsed(void);
extern uint32_t Get_MaxFreeBLOCKSize(void);
extern uint32_t Get_MinFreeBLOCKSize(void);


#ifdef __cplusplus
}
#endif

#endif // __AK_HEAP_H__
