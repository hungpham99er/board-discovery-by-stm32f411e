include sources/ak/ak.cfg.mk

CFLAGS += -Isources/ak/inc
CPPFLAGS += -Isources/ak/inc

AK_SOURCE_PATH = sources/ak/src

# C source files
C_SOURCES += $(AK_SOURCE_PATH)/fsm.c
C_SOURCES += $(AK_SOURCE_PATH)/tsm.c
C_SOURCES += $(AK_SOURCE_PATH)/task.c
C_SOURCES += $(AK_SOURCE_PATH)/timer.c
C_SOURCES += $(AK_SOURCE_PATH)/message.c
C_SOURCES += $(AK_SOURCE_PATH)/heap.c
