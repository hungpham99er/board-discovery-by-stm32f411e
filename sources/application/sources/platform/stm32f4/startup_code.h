#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#ifdef __cplusplus
extern "C"
{
#endif

/*****************************************
 * @define: STARTUP CODE GENERAL DEFINES *
 *****************************************/
#define __DEFAULT__ __attribute__((weak, alias("Default_Handler")))

#ifdef __cplusplus
}
#endif

#endif //__SYSTEM_H__
