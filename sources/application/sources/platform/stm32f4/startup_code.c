#include <stdint.h>

#include "app.h"
#include "startup_code.h"
#include "stm32f4xx_rcc.h"
#include "system_stm32f4xx.h"

#include "sys_dbg.h"
#include "sys_cfg.h"
#include "sys_flash.h"

#include "io_cfg.h"

#include "platform.h"

/************************************
 * @brief EXTERN & GLOBAL VARIABLES *
 ************************************/
extern uint32_t _sstack;
extern uint32_t _estack;
extern uint32_t _sidata;
extern uint32_t _sdata;
extern uint32_t _edata;
extern uint32_t _sbss; 
extern uint32_t _ebss;

extern void (*__preinit_array_start[])();
extern void (*__preinit_array_end[])();
extern void (*__init_array_start[])();
extern void (*__init_array_end[])();

extern FatalLog_t Fatal_Log_Region;

/****************************************
 * @brief: PRIVATE FUNCTION PROTOTYPES  *
 ****************************************/
static void prvSystem_ResetReason(void);

/****************************************
 * @func: INTERRUPT FUNCTION PROTOTYPES *
 ****************************************/
void Default_Handler(void);

void Reset_IRQHandler(void);
__DEFAULT__ void NMI_IRQHandler(void);
__DEFAULT__ void SVC_IRQHandler(void);
__DEFAULT__ void DebugMonitor_IRQHandler(void);
__DEFAULT__ void PendSV_IRQHandler(void);
__DEFAULT__ void SysTick_IRQHandler(void);

__DEFAULT__ void HardFault_IRQHandler(void);
__DEFAULT__ void MemManageFault_IRQHandler(void);
__DEFAULT__ void BusFault_IRQHandler(void);
__DEFAULT__ void UsageFault_IRQHandler(void);

__DEFAULT__ void EXT0_IRQHanler(void);
__DEFAULT__ void EXTI9_5_IRQHandler(void);
__DEFAULT__ void USART1_IRQHandler(void);
__DEFAULT__ void USART2_IRQHandler(void);
__DEFAULT__ void ADC_IRQHandler(void);
__DEFAULT__ void TIM2_IRQHandler(void);
__DEFAULT__ void TIM3_IRQHandler(void);
__DEFAULT__ void TIM4_IRQHandler(void);
__DEFAULT__ void TIM5_IRQHandler(void);
__DEFAULT__ void TIM1_BRK_TIM9_IRQHandler(void);
__DEFAULT__ void TIM1_UPT_TIM10_IRQHandler(void);
__DEFAULT__ void RTC_WKUP_IRQHandler(void);
__DEFAULT__ void RTC_Alarm_IRQHandler(void);
__DEFAULT__ void TAMP_STAMP_IRQHandler(void);
__DEFAULT__ void DMA1_Stream0_IRQHandler(void);
__DEFAULT__ void DMA1_Stream1_IRQHandler(void);
__DEFAULT__ void DMA1_Stream2_IRQHandler(void);
__DEFAULT__ void DMA1_Stream3_IRQHandler(void);
__DEFAULT__ void DMA1_Stream4_IRQHandler(void);
__DEFAULT__ void DMA1_Stream5_IRQHandler(void);
__DEFAULT__ void DMA1_Stream6_IRQHandler(void);
__DEFAULT__ void DMA1_Stream7_IRQHandler(void);
__DEFAULT__ void DMA2_Stream0_IRQHandler(void);
__DEFAULT__ void DMA2_Stream1_IRQHandler(void);
__DEFAULT__ void DMA2_Stream2_IRQHandler(void);
__DEFAULT__ void DMA2_Stream3_IRQHandler(void);
__DEFAULT__ void DMA2_Stream4_IRQHandler(void);
__DEFAULT__ void DMA2_Stream5_IRQHandler(void);
__DEFAULT__ void DMA2_Stream6_IRQHandler(void);
__DEFAULT__ void DMA2_Stream7_IRQHandler(void);


__attribute__((section(".isr_vector"))) void (*const isr_vector[])() = {
	(void (*)(void)) & _estack, /*      Initialize stack pointer */
	Reset_IRQHandler,           /*      Reset Handler           */
	NMI_IRQHandler,             /*      NMI Handler             */
	HardFault_IRQHandler,       /*      HardFault Handler       */
	MemManageFault_IRQHandler,  /*      MemManageFault Handler  */
	BusFault_IRQHandler,        /*      BusFault Handler        */
	UsageFault_IRQHandler,      /*      UsageFault Handler      */
	0,                          /*      Reserved                */
	0,                          /*      Reserved                */
	0,                          /*      Reserved                */
	0,                          /*      Reserved                */
	SVC_IRQHandler,             /*      SVC Handler             */
	DebugMonitor_IRQHandler,    /*      DebugMoniter Handler    */
	0,                          /*      Reserved                */
	PendSV_IRQHandler,          /*      PendSV Handler          */
	SysTick_IRQHandler,         /*      Systick Handler         */

	Default_Handler,			/*      Window Watchdog interrupt */
	Default_Handler,			/*      EXTI Line 16 interrupt / PVD through EXTI line detection interrupt    */
	TAMP_STAMP_IRQHandler,		/*      EXTI Line 21 interrupt / Tamper and TimeStamp interrupts through the EXTI line   */
	RTC_WKUP_IRQHandler,		/*      RTC Wakeup through EXTI line interrupt		*/
	Default_Handler,			/*      Flash global interrupt						*/
	Default_Handler,			/*      RCC global interrupt						*/
	EXT0_IRQHanler,				/*      EXTI Line0 interrupt						*/
	Default_Handler,			/*      EXTI Line1 interrupt						*/
	Default_Handler,			/*      EXTI Line2 interrupt						*/
	Default_Handler,			/*      EXTI Line3 interrupt						*/
	Default_Handler,			/*      EXTI Line4 interrupt						*/
	DMA1_Stream0_IRQHandler,	/*      DMA1 Stream0 global interrupt				*/
	DMA1_Stream1_IRQHandler,	/*      DMA1 Stream1 global interrupt				*/
	DMA1_Stream2_IRQHandler,	/*      DMA1 Stream2 global interrupt				*/
	DMA1_Stream3_IRQHandler,	/*      DMA1 Stream3 global interrupt				*/
	DMA1_Stream4_IRQHandler,	/*      DMA1 Stream4 global interrupt				*/
	DMA1_Stream5_IRQHandler,	/*      DMA1 Stream5 global interrupt				*/
	DMA1_Stream6_IRQHandler,	/*      DMA1 Stream6 global interrupt				*/
	ADC_IRQHandler,				/*      ADC global interrupt						*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	EXTI9_5_IRQHandler,			/*      EXTI Line [9:5] interrupt					*/
	TIM1_BRK_TIM9_IRQHandler,	/*      TIM1 Break interrupt and TIM9 global interrupt			*/
	TIM1_UPT_TIM10_IRQHandler,	/*      TIM1 Update interrupt and TIM10 global interrupt		*/
	Default_Handler,			/*      TIM1 Trigger and Commutation interrupts and TIM11 global interrupt	*/
	Default_Handler,			/*      TIM1 Capture Compare interruptTIM2 global interrupt		*/
	TIM2_IRQHandler,			/*      TIM2 global interrupt						*/
	TIM3_IRQHandler,			/*      TIM3 global interrupt						*/
	TIM4_IRQHandler,			/*      TIM4 global interrupt						*/
	Default_Handler,			/*      I2C1 event interrupt						*/
	Default_Handler,			/*      I2C1 error interrupt						*/
	Default_Handler,			/*      I2C2 event interrupt						*/
	Default_Handler,			/*      I2C2 error interrupt						*/
	Default_Handler,			/*      SPI1 global interrupt						*/
	Default_Handler,			/*      SPI2 global interrupt						*/
	USART1_IRQHandler,			/*      USART1 global interrupt						*/
	USART2_IRQHandler,			/*      USART2 global interrupt						*/
	0,							/*      Reserved									*/
	Default_Handler,			/*      EXTI Line[15:10] interrupts					*/
	RTC_Alarm_IRQHandler,		/*      EXTI Line 17 interrupt / RTC Alarms (A and B) through EXTI line interrupt    */
	Default_Handler,			/*      EXTI Line 18 interrupt / USB On-The-Go FS Wakeup through EXTI line interrupt */
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	DMA1_Stream7_IRQHandler,	/*      DMA1 Stream7 global interrupt				*/
	0,							/*      Reserved									*/
	Default_Handler,			/*      SDIO global interrupt						*/
	TIM5_IRQHandler,			/*      TIM5 global interrupt						*/
	Default_Handler,			/*      SPI3 global interrupt						*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	DMA2_Stream0_IRQHandler,	/*      DMA2 Stream0 global interrupt				*/
	DMA2_Stream1_IRQHandler,	/*      DMA2 Stream1 global interrupt				*/
	DMA2_Stream2_IRQHandler,	/*      DMA2 Stream2 global interrupt				*/
	DMA2_Stream3_IRQHandler,	/*      DMA2 Stream3 global interrupt				*/
	DMA2_Stream4_IRQHandler,	/*      DMA2 Stream4 global interrupt				*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	Default_Handler,			/*      USB On The Go FS global interrupt			*/
	DMA2_Stream5_IRQHandler,	/*      DMA2 Stream5 global interrupt				*/
	DMA2_Stream6_IRQHandler,	/*      DMA2 Stream6 global interrupt				*/
	DMA2_Stream7_IRQHandler,	/*      DMA2 Stream7 global interrupt				*/
	Default_Handler,			/*      USART6 global interrupt						*/
	Default_Handler,			/*      I2C3 event interrupt						*/
	Default_Handler,			/*      I2C3 error interrupt						*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	Default_Handler,			/*      FPU global interrupt						*/
	0,							/*      Reserved									*/
	0,							/*      Reserved									*/
	Default_Handler,			/*      SPI4 global interrupt						*/
	Default_Handler,			/*      SPI5 global interrupt						*/
};


void Reset_IRQHandler() {
	/* MUST BE disable interrupt */
	__disable_irq();

	volatile uint32_t *__src = &_sidata;
	volatile uint32_t *__des;
	volatile unsigned i, cnt;

	/* Init system */
	SystemInit();

	/* MUST-BE Remap vector table address after Jump to App address */
	SCB->VTOR = APP_START_ADDR;

	/* Copying of the .data values into SRAM */
	for (__des = &_sdata; __des < &_edata;) {
		*(__des++) = *(__src++);
	}

	/* Initializing .bss values to zero*/
	for (__des = &_sbss; __des < &_ebss;) {
		*(__des++) = 0U;
	}

	/* Invoke all static constructors */
	cnt = __preinit_array_end - __preinit_array_start;
	for (i = 0; i < cnt; i++) {
		__preinit_array_start[i]();
	}

	cnt = __init_array_end - __init_array_start;
	for (i = 0; i < cnt; i++) {
		__init_array_start[i]();
	}

	SystemAPP_ClockConfig();
	SystemAPP_TickConfig();
	SystemAPP_ShellConfig();

#if 0
	FatalLog_t SystemLog;
	APP_InternalFlashCopyDataTo(APP_FLASH_LOG_FATAL_INFO_SECTOR,
								&SystemLog, 
								sizeof(FatalLog_t)
								);
	++(SystemLog.RestartTimes);
	APP_InternalFlashWriteByte(APP_FLASH_LOG_FATAL_INFO_SECTOR, 
                                (uint8_t *)&SystemLog, 
                                sizeof(FatalLog_t)
                                );

#endif

	SystemAPP_UpdateInformation();
	prvSystem_ResetReason();

	__enable_irq();

	xfunc_output = (void(*))SystemAPP_IRQShellPutChar;

	/* Go to main function */
	main_app();
}

void LSI_SetUp() {

}


void prvSystem_ResetReason() {
	if (RCC_GetFlagStatus(RCC_FLAG_BORRST) == SET) {
		SYS_PRINT ("[RESET BY] POR/PDR_BOR_RST");
	}
	else if (RCC_GetFlagStatus(RCC_FLAG_PORRST) == SET) {
		SYS_PRINT ("[RESET BY] POR/PDR_RST");
	}
	else if (RCC_GetFlagStatus(RCC_FLAG_SFTRST) == SET) {
		SYS_PRINT ("[RESET BY] SFTRST");
	}
	else if (RCC_GetFlagStatus(RCC_FLAG_IWDGRST) == SET) {
		SYS_PRINT ("[RESET BY] IWDGRST");
	}
	else if (RCC_GetFlagStatus(RCC_FLAG_WWDGRST) == SET) {
		SYS_PRINT ("[RESET BY] WWDGRST");
	}
	else if (RCC_GetFlagStatus(RCC_FLAG_LPWRRST) == SET) {
		SYS_PRINT ("[RESET BY] LPWRRST");
	}
	else if (RCC_GetFlagStatus(RCC_FLAG_PINRST) == SET) {
		SYS_PRINT ("[RESET BY] PINRST");
	}
	else {
		SYS_PRINT ("UNKNOWN");
	}
	
	SYS_PRINT ("\n");
	RCC_ClearFlag();
}

void Default_Handler() {

}
