#include <stdint.h>
#include <stdbool.h>

#include "timer.h"

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#include "xprintf.h"
#include "ring_buffer.h"

#include "sys_dbg.h"
#include "sys_cfg.h"
#include "sys_ctrl.h"
#include "sys_dbg.h"
#include "ak.h"

#include "io_cfg.h"

/************************************
 * @brief EXTERN & GLOBAL VARIABLES *
 ************************************/
extern ring_buffer_char_t SysShellSendChar;
extern ring_buffer_char_t SerialBufferRead;

/****************************
 * @brief PRIVATE VARIABLES *
 ****************************/
static volatile uint32_t s_MsTicks = 0;
static volatile uint32_t s_UsTicks = 0;


/**********************************************
 * @brief CORTEX-M PROCESSOR FAULT-EXCEPTIONS *
 **********************************************/
void NMI_IRQHandler() {
	FATAL("SY", 0x01);
}
void HardFault_IRQHandler() {
	FATAL("SY", 0x02);
}
void MemManageFault_IRQHandler() {
	FATAL("SY", 0x03);
}
void BusFault_IRQHandler() {
	FATAL("SY", 0x04);
}
void UsageFault_IRQHandler() {
	FATAL("SY", 0x05);
}

/**************************************************
 * @brief CORTEX-M PROCESSOR NON-FAULT-EXCEPTIONS *
 **************************************************/
void SysTick_IRQHandler() {
    TASK_ENTRY_IRQ();

    /* Increase MILLIS seconds */
	++(s_MsTicks);

    /* Handle tick of task timer */
    timer_tick(1);

    static uint8_t Pol_Btn_Cnt = 0;
    ++(Pol_Btn_Cnt);
    if (Pol_Btn_Cnt == 10) {
        AppPollingButton();
        Pol_Btn_Cnt = 0;
    }

    TASK_EXIT_IRQ();
}

uint32_t MILLIS() {
	uint32_t Ret = 0;

	ENTRY_CRITICAL();
	Ret = s_MsTicks;
	EXIT_CRITICAL();

	return Ret;
}

uint32_t MICROS() {
	ENTRY_CRITICAL();
	uint32_t m = s_MsTicks;
	EXIT_CRITICAL();

	const uint32_t tms = SysTick->LOAD + 1;
	__IO uint32_t u = tms - SysTick->VAL;

	/* Checks if the Systick counter flag is active or not */
	if (((SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) == (SysTick_CTRL_COUNTFLAG_Msk))) {
		m = s_MsTicks;
		u = tms - SysTick->VAL;
	}

	return (m * 1000 + (u * 1000) / tms);
}

/***************************************
 * @brief CORTEX-M PROCESSOR INTERRUPT *
 ***************************************/
void USART2_IRQHandler() {
	uint8_t ByteTxRx;

    TASK_ENTRY_IRQ();

	/* UART_2 Received */
	if (USART_GetITStatus(SYS_SHELL_UART, USART_IT_RXNE) == SET) {
		ENTRY_CRITICAL();
		ByteTxRx = SystemAPP_IRQShellGetChar();        
        EXIT_CRITICAL();

		AppPollingShell(ByteTxRx);
	}

	/* UART_2 Transmit */
	if (USART_GetITStatus(SYS_SHELL_UART, USART_IT_TXE) == SET) {
		if (!ring_buffer_char_is_empty(&SysShellSendChar)) {
			ByteTxRx = ring_buffer_char_get(&SysShellSendChar);
			USART_SendData(SYS_SHELL_UART, ByteTxRx);
		}
		else {
			USART_ITConfig(SYS_SHELL_UART, USART_IT_TXE, DISABLE);
		}
	}

    TASK_EXIT_IRQ();
}
