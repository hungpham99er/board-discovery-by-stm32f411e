#ifndef __IO_CFG_H__
#define __IO_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "system_stm32f4xx.h"


/*********************************
 * @define: LED PIN MAP DEFINES  *
 *********************************/
#define USER_LED_CLOCK_GATE			( RCC_AHB1Periph_GPIOD )
#define LED_LIFE    				( GPIO_Pin_12 )
#define USER_ORANGE_LED				( GPIO_Pin_13 )
#define USER_RED_LED				( GPIO_Pin_14 )
#define USER_BLUE_LED				( GPIO_Pin_15 )
#define USER_LED_MASK				( USER_ORANGE_LED | USER_RED_LED | USER_BLUE_LED )
#define USER_LED_PORT				( GPIOD )

extern void LifeLED_Config(void);
extern void LifeLED_On(void);
extern void LifeLED_Off(void);

extern void UserLEDs_Config();
extern void UserLEDs_On(uint16_t user_led);
extern void UserLEDs_Off(uint16_t user_led);
extern void UserLEDs_Blink(uint16_t user_led);

/************************************
 * @define: BUTTON PIN MAP DEFINES  *
 ************************************/
#define USER_BUTTON_CLOCK_GATE		( RCC_AHB1Periph_GPIOA )
#define USER_BUTTON_PIN				( GPIO_Pin_0 )
#define USER_BUTTON_PORT	        ( GPIOA )

extern void     UserButton_Config(void);
extern uint8_t  UserButton_Reading(void);


#ifdef __cplusplus
}
#endif

#endif //__IO_CFG_H__
