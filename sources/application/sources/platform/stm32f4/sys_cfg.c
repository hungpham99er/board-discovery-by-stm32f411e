#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>

#include "sys_cfg.h"
#include "startup_code.h"

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "system_stm32f4xx.h"

#include "xprintf.h"
#include "ring_buffer.h"

#include "sys_dbg.h"
#include "sys_ctrl.h"
#include "sys_flash.h"


/************************************
 * @brief EXTERN & GLOBAL VARIABLES *
 ************************************/
ring_buffer_char_t SysShellSendChar;
uint8_t BufferCharSend[BUFFER_CHAR_SHELL_SEND_SIZE];

/********************************************************
 * 														*
 * 														*
 * 														*
 * 					SYSTEM CONFIGURATIONS				*
 * 														*
 *  													*
 *  													*
 ********************************************************/
void SystemAPP_ClockConfig() {
    SystemCoreClockUpdate();
    RCC_LSICmd(ENABLE);
	while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET) {
        /* YEILD */
	}
}

void SystemAPP_TickConfig() {
    SysTick_Config(SystemCoreClock / 1000);
}


void SystemAPP_ShellConfig() {
    GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(SYS_SHELL_UART_CLOCK, ENABLE);
	RCC_AHB1PeriphClockCmd(SYS_SHELL_IO_PORT_CLOCK, ENABLE);

	GPIO_PinAFConfig(SYS_SHELL_IO_PORT, SYS_SHELL_TX_PIN_SOURCE, SYS_SHELL_IO_AF);
	GPIO_PinAFConfig(SYS_SHELL_IO_PORT, SYS_SHELL_RX_PIN_SOURCE, SYS_SHELL_IO_AF);

	GPIO_InitStructure.GPIO_Pin = SYS_SHELL_TX_PIN;;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(SYS_SHELL_IO_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = SYS_SHELL_RX_PIN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(SYS_SHELL_IO_PORT, &GPIO_InitStructure);

	USART_InitStructure.USART_BaudRate = 460800;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(SYS_SHELL_UART, &USART_InitStructure);

    USART_ITConfig (SYS_SHELL_UART, USART_IT_RXNE, ENABLE);
	USART_ITConfig (SYS_SHELL_UART, USART_IT_TXE, DISABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = SYS_SHELL_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_Cmd(SYS_SHELL_UART, ENABLE);

	ring_buffer_char_init(&SysShellSendChar, BufferCharSend, BUFFER_CHAR_SHELL_SEND_SIZE);
    xfunc_output = (void(*))SystemAPP_ShellPutChar;
}

void SystemAPP_UpdateInformation() {
    extern uint32_t _start_flash;
    extern uint32_t _end_flash;
    extern uint32_t _start_sram;
    extern uint32_t _end_sram;
    extern uint32_t _sdata;
    extern uint32_t _edata;
    extern uint32_t _sbss;
    extern uint32_t _ebss;
    extern uint32_t __heap_start__;
    extern uint32_t __heap_end__;
    extern uint32_t _estack;

    /* Get clock info */
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq(&RCC_Clocks);

    /* Kernel banner */
    SYS_PRINT("\n");
    SYS_PRINT("   __    _  _ \n");
    SYS_PRINT("  /__\\  ( )/ )\n");
    SYS_PRINT(" /(__)\\ (   ( \n");
    SYS_PRINT("(__)(__)(_)\\_)\n");
    SYS_PRINT("Wellcome to Active Kernel %s\n", AK_VERSION);
    SYS_PRINT("\n");

#if 1
    SYS_PRINT("======== System Information =========\n");
    SYS_PRINT("\tFLASH used:\t%d bytes\n", (((uint32_t)&_end_flash - (uint32_t)&_start_flash) + \
                                            ((uint32_t)&_edata - (uint32_t)&_sdata)));
    SYS_PRINT("\tSRAM used:\t%d bytes\n", ((uint32_t)&_end_sram - (uint32_t)&_start_sram));
    SYS_PRINT("\t\tdata init size:\t\t%d bytes\n", (uint32_t)&_edata - (uint32_t)&_sdata);
    SYS_PRINT("\t\tdata non_init size:\t%d bytes\n", (uint32_t)&_ebss - (uint32_t)&_sbss);
    SYS_PRINT("\t\tstack avail:\t\t%d bytes\n", (uint32_t)&_estack - (uint32_t)&_start_sram);
    SYS_PRINT("\t\theap avail:\t\t%d bytes\n", (uint32_t)&__heap_end__ - (uint32_t)&__heap_start__);
    SYS_PRINT("\n");
    SYS_PRINT("\tcpu clock:\t%d Hz\n", RCC_Clocks.HCLK_Frequency);
    SYS_PRINT("\tPCLK1 clock:\t%d Hz\n", RCC_Clocks.PCLK1_Frequency);
    SYS_PRINT("\ttime tick:\t1 ms\n");
    SYS_PRINT("\tconsole:\t460800 bps\n");
    SYS_PRINT("\n");
#endif
}

/********************************************************
 * 														*
 * 														*
 * 														*
 * 					SYSTEM CONTROL						*
 * 														*
 *  													*
 *  													*
 ********************************************************/
void SystemAPP_Reset() {
    NVIC_SystemReset();
}

/**
 * ******************************************************************************
 * @function: SystemAPP_IWDGConfig                                              *
 * @brief: Timeout after 20s                                                    *
 * @formula:                                                                    *
 * IWDG counter clock: 32KHz(LSI) / 256 = 125 Hz                                *
 * Set counter reload, T = (1/IWDG counter clock) * Reload_counter  = 20s       *
 ********************************************************************************/                                 
void SystemAPP_IWDGConfig() {
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	IWDG_SetPrescaler(IWDG_Prescaler_256);
	IWDG_SetReload(2500); /* MUST-BE in range 0-4095 */
	IWDG_ReloadCounter();

	IWDG_Enable();
}

void SystemAPP_IWDGRefresh() {
    ENTRY_CRITICAL();
	IWDG_ReloadCounter();
    EXIT_CRITICAL();
}

void DELAY_TICK_Ms(uint32_t time_out_ms) {
    uint32_t now = MILLIS();
    while (MILLIS() - now < time_out_ms) {
        /* YIELD() */
    }
}

void DELAY_TICK_Us(uint32_t time_out_us) {
#if 1 
    __IO uint32_t currentTicks = SysTick->VAL;
	/* Number of ticks per MILLISecond */
	const uint32_t tickPerMs = SysTick->LOAD + 1;
	/* Number of ticks to count */
	const uint32_t nbTicks = ((time_out_us - ((time_out_us > 0) ? 1 : 0)) * tickPerMs) / 1000;
	/* Number of elapsed ticks */
	uint32_t elapsedTicks = 0;
	__IO uint32_t oldTicks = currentTicks;
	do {
		currentTicks = SysTick->VAL;
		elapsedTicks += (oldTicks < currentTicks) ? tickPerMs + oldTicks - currentTicks :
													oldTicks - currentTicks;
		oldTicks = currentTicks;
	} while (nbTicks > elapsedTicks);
#else 
    uint32_t now = MICROS();
    while (MICROS() - now < time_out_us) {
        /* YIELD() */
    }

#endif
}

uint32_t SystemAPP_GetExceptionNumber() {
	return (uint32_t)__get_IPSR();;
}

uint32_t SystemAPP_CRC_CalcCRC(uint32_t __num) {
    uint32_t _Ret = 0;

    /* Open CLOCK-GATING */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, ENABLE);

    ENTRY_CRITICAL();
    /* Compute the CRC */
    _Ret = CRC_CalcCRC( __num );
    EXIT_CRITICAL();

    /* Close CLOCK-GATING */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, DISABLE);

    return ( _Ret );
}

uint8_t SystemAPP_IRQShellGetChar() {
    uint8_t c = 0;
    if (USART_GetITStatus(SYS_SHELL_UART, USART_IT_RXNE) == SET) {
        USART_ClearITPendingBit(SYS_SHELL_UART, USART_IT_RXNE);
        c = (uint8_t)USART_ReceiveData(SYS_SHELL_UART);
    }
	return c;
}

void SystemAPP_IRQShellPutChar(uint8_t c) {
    ENTRY_CRITICAL();
    ring_buffer_char_put(&SysShellSendChar, c);
    EXIT_CRITICAL();

    USART_ITConfig (SYS_SHELL_UART, USART_IT_TXE, ENABLE);
}

void SystemAPP_ShellPutChar(uint8_t c) {
	while (RESET == USART_GetFlagStatus(SYS_SHELL_UART, USART_FLAG_TXE));
	USART_SendData(SYS_SHELL_UART, c);
	while (RESET == USART_GetFlagStatus(SYS_SHELL_UART, USART_FLAG_TC));
}

uint8_t SystemAPP_ShellGetChar() {
	uint8_t c = 0;

    while(RESET == USART_GetFlagStatus(SYS_SHELL_UART, USART_FLAG_RXNE)) {

    }
	c = (uint8_t)USART_ReceiveData(SYS_SHELL_UART);

	return c;
}

void APP_InternalFlashEraseSector(uint32_t __addr, uint32_t __len) {
    uint32_t StartOfSector;
    uint32_t EndOfSector, SectorCounter;

    /* Enable the flash control register access */
    FLASH_Unlock();

    /* Clear pending flags (if any) */  
    FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | 
                  FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR); 

    StartOfSector = APP_GetFlashSectorNumber(__addr);
    EndOfSector = APP_GetFlashSectorNumber(__addr + __len);

    /* Strat the erase operation */
    SectorCounter = StartOfSector;
    while (SectorCounter <= EndOfSector) {
        if (FLASH_EraseSector(SectorCounter, VoltageRange_3) != FLASH_COMPLETE) { 
            /**
             * Error occurred while sector erase. 
             * User can add here some code to deal with this error
             ****/
            while (1) {
            }
        }
        /* Jump to the next sector */
        SectorCounter += NEXT_SECTOR_STEP_UNIT;
    }

    /**
     * Lock the Flash to disable the flash control register access (recommended
     * to protect the FLASH memory against possible unwanted operation) 
     ***/
    FLASH_Lock(); 

}

uint8_t APP_InternalFlashWriteByte(uint32_t __addr, uint8_t *__data, uint32_t __len) {
    /* Enable the flash control register access */
    FLASH_Unlock();

    /* Clear pending flags (if any) */  
    FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | 
                    FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR
                    ); 

    uint8_t Memory_Program_Status = 0;
    uint32_t iDx = 0;
    uint8_t cpyData;
    uint8_t *__paddr = (uint8_t *)(uint32_t *)__addr;

#if 1
    do {
        cpyData = __data[iDx];

        Memory_Program_Status = FLASH_ProgramByte((uint32_t)__paddr, cpyData);
        
        if (Memory_Program_Status != FLASH_COMPLETE) {
            /* Write to FLASH failed */

            /* Clear pending flags (if any) */  
            FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | 
                            FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR
                            ); 
        }

        ++__paddr;
        ++iDx;
    }
     while (iDx < __len);
#endif

#if 0
    while (iDx < __len) {
        cpyData = 0;
        memcpy(&cpyData, &__data[iDx], (__len - iDx) >= sizeof(uint32_t) ? sizeof(uint32_t) : (__len - iDx));

        Memory_Program_Status = FLASH_ProgramWord(__addr + iDx, cpyData);
        if (Memory_Program_Status == FLASH_COMPLETE) {
            iDx += sizeof(uint32_t);
        }
        else {
            /* Write to FLASH failed */

            /* Clear pending flags (if any) */  
            FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | 
                        FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR); 
        }
    }
#endif

    /**
     * Lock the Flash to disable the flash control register access (recommended
     * to protect the FLASH memory against possible unwanted operation) 
     ***/
    FLASH_Lock(); 

    return ( Memory_Program_Status );
}

void APP_InternalFlashCopyDataTo(uint32_t __addr, void *__buf, uint32_t __len) {
    memcpy(__buf, (uint32_t *)(__addr), __len);
}

uint32_t APP_GetFlashSectorNumber(uint32_t __addr) {
    uint32_t _Ret = 0;
  
    if((__addr < ADDR_FLASH_SECTOR_1) && (__addr >= ADDR_FLASH_SECTOR_0)) {
        _Ret = FLASH_Sector_0;  
    }
    else if((__addr < ADDR_FLASH_SECTOR_2) && (__addr >= ADDR_FLASH_SECTOR_1)) {
        _Ret = FLASH_Sector_1;  
    }
    else if((__addr < ADDR_FLASH_SECTOR_3) && (__addr >= ADDR_FLASH_SECTOR_2)) {
        _Ret = FLASH_Sector_2;  
    }
    else if((__addr < ADDR_FLASH_SECTOR_4) && (__addr >= ADDR_FLASH_SECTOR_3)) {
        _Ret = FLASH_Sector_3;  
    }
    else if((__addr < ADDR_FLASH_SECTOR_5) && (__addr >= ADDR_FLASH_SECTOR_4)) {
        _Ret = FLASH_Sector_4;  
    }
    else if((__addr < ADDR_FLASH_SECTOR_6) && (__addr >= ADDR_FLASH_SECTOR_5)) {
        _Ret = FLASH_Sector_5;  
    }
    else if((__addr < ADDR_FLASH_SECTOR_7) && (__addr >= ADDR_FLASH_SECTOR_6)) {
        _Ret = FLASH_Sector_6;  
    }
    else if((__addr < ADDR_FLASH_SECTOR_END) && (__addr >= ADDR_FLASH_SECTOR_7)) {
        _Ret = FLASH_Sector_7;  
    }

    return ( _Ret );
}


/********************************************************
 * 														*
 * 														*
 * 														*
 * 					ACTIVE KERNAL						*
 * 														*
 *  													*
 *  													*
 ********************************************************/
void TASK_IRQ_IO_ENTRY_TRIGGER() {
#if defined(AK_IO_IRQ_ANALYZER)
	GPIO_ResetBits(USER_LED_PORT, LED_LIFE);
#endif
}

void TASK_IRQ_IO_EXIT_TRIGGER() {
#if defined(AK_IO_IRQ_ANALYZER)
	GPIO_SetBits(USER_LED_PORT, LED_LIFE);
#endif
}
