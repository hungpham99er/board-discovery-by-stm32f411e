#include <stdint.h>
#include <stdbool.h>

#include "io_cfg.h"
#include "platform.h"

#include "sys_dbg.h"
#include "sys_ctrl.h"

#include "app_dbg.h"

#include "startup_code.h"


/********************************************************
 * 														*
 * 														*
 * 														*
 * 						LED FUNTIONS					*
 * 														*
 *  													*
 *  													*
 ********************************************************/
void LifeLED_Config() {
    GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(USER_LED_CLOCK_GATE, ENABLE);

	GPIO_InitStructure.GPIO_Pin = LED_LIFE;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(USER_LED_PORT, &GPIO_InitStructure);
}

void LifeLED_On() {
    GPIO_ResetBits(USER_LED_PORT, LED_LIFE);
}

void LifeLED_Off() {
    GPIO_SetBits(USER_LED_PORT, LED_LIFE);
}

void UserLEDs_Config() {
    GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(USER_LED_CLOCK_GATE, ENABLE);

	GPIO_InitStructure.GPIO_Pin = USER_LED_MASK;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(USER_LED_PORT, &GPIO_InitStructure);
}

void UserLEDs_On(uint16_t user_led) {
    GPIO_ResetBits(USER_LED_PORT, user_led);
}

void UserLEDs_Off(uint16_t user_led) {
    GPIO_SetBits(USER_LED_PORT, user_led);
}

void UserLEDs_Blink(uint16_t user_led) {
    GPIO_ToggleBits(USER_LED_PORT, user_led);
}


/********************************************************
 * 														*
 * 														*
 * 														*
 * 						BUTTON FUNTIONS					*
 * 														*
 *  													*
 *  													*
 ********************************************************/
void UserButton_Config() {
    GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(USER_BUTTON_CLOCK_GATE, ENABLE);

	GPIO_InitStructure.GPIO_Pin = USER_BUTTON_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(USER_BUTTON_PORT, &GPIO_InitStructure);

}

uint8_t UserButton_Reading() {
    return GPIO_ReadInputDataBit(USER_BUTTON_PORT, USER_BUTTON_PIN);
}
