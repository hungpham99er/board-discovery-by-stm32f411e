/**
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 * @author: ThanNT				@
 * @date:   05/09/2016			@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
**/
#include <stdint.h>

#include "platform.h"
#include "sys_dbg.h"
#include "stm32f4xx.h"

/****************************
 * @brief PRIVATE VARIABLES *
 ****************************/
static int Nest_Entry_Critical_Counter = 0;

void EnableIRQ() {
	--(Nest_Entry_Critical_Counter);
	__enable_irq();
}

void DisableIRQ() {
	__disable_irq();
	++(Nest_Entry_Critical_Counter);
}

void EntryCritical() {
	if (Nest_Entry_Critical_Counter == 0) {
		__disable_irq();
	}
	Nest_Entry_Critical_Counter++;
}

void ExitCritical() {
	Nest_Entry_Critical_Counter--;
	if (Nest_Entry_Critical_Counter == 0) {
		Nest_Entry_Critical_Counter = 0;	/* prevent call ExitCritical many times */
		__enable_irq();
	}
	else if (Nest_Entry_Critical_Counter < 0) {
		FATAL("ITR", 0x01);
	}
}

int GetNestEntryCriticalCounter() {
	return Nest_Entry_Critical_Counter;
}