#ifndef __SYS_DBG_H__
#define __SYS_DBG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include "task.h"
#include "message.h"
#include "sys_ctrl.h"
#include "platform.h"
#include "xprintf.h"

/*****************************************
 * @define: SYSTEM DEBUG GENERAL DEFINES *
 *****************************************/
#if defined(SYS_DBG_EN)
#define SYS_DBG(fmt, ...)       xprintf((const char*)fmt, ##__VA_ARGS__)
#else
#define SYS_DBG(fmt, ...)
#endif

#if defined(SYS_PRINT_EN)
#define SYS_PRINT(fmt, ...)       xprintf((const char*)fmt, ##__VA_ARGS__)
#else
#define SYS_PRINT(fmt, ...)
#endif

#define SYS_IRQ_EXCEPTION_NUMBER_IRQ0_NUMBER_RESPECTIVE		( 16 )	/* exception number 16 ~~ IRQ0 */

typedef struct CORE_REG {
    uint32_t IPSR;          /* Interrupt Program Status Register */
    uint32_t PRIMASK;       /* Priority Mask Register */
    uint32_t CONTROL_REG;   /* CONTROL register */
} Core_Reg_t;

typedef struct FATAL_LOG {
    int8_t String[10];
    uint8_t Code;
    task_t CurrentTask;
    ak_msg_t CurrentActiveObject;
    Core_Reg_t Core_Reg;
    uint8_t FatalTimes;
    uint8_t RestartTimes;
} __AK_PACKETED FatalLog_t;


#define FATAL(s, c)                                 \
    do {                                            \
        DISABLE_INTERRUPTS();                       \
        SystemFatal((const int8_t*)s, (uint8_t)c);  \
    } while (0)


/**
 * @brief SYSTEM DEBUG FUNCTION PROTOTYPES
 * 
 * @param s 
 * @param c 
 */
extern void SystemFatal(const int8_t* s, uint8_t c);

#ifdef __cplusplus
}
#endif

#endif /* __SYS_DBG_H__ */