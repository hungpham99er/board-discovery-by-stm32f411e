﻿#include "ak.h"

#include "sys_dbg.h"
#include "sys_ctrl.h"
#include "sys_flash.h"

#include "sys_cfg.h"
#include "io_cfg.h"

void SystemFatal(const int8_t* s, uint8_t c) {
    FatalLog_t SystemLog;
    uint8_t Keyw = 0;
    (void)Keyw;

    memset(&SystemLog, 0, sizeof(FatalLog_t));
    xfunc_output = (void(*))SystemAPP_ShellPutChar;
    
    SYS_PRINT("[FATAL] %s\t%x\n", s, c);

    /* Increase fatal times */
    ++(SystemLog.FatalTimes);

    /* Set fatal string & code */
    memset(SystemLog.String, 0, 10);
    strcpy((char *)SystemLog.String, (const char * )s);
    SystemLog.Code = c;

    /* Get current task & current active object */
    memcpy(&SystemLog.CurrentTask, (uint8_t *)get_current_task_info(), sizeof(task_t));
    memcpy(&SystemLog.CurrentActiveObject, (uint8_t *)get_current_active_object(), sizeof(ak_msg_t));

    /* Get core register */
    SystemLog.Core_Reg.IPSR = __get_IPSR();
    SystemLog.Core_Reg.PRIMASK = __get_PRIMASK();
    SystemLog.Core_Reg.CONTROL_REG = __get_CONTROL();

    /* Save FATAL LOG into FLASH */
    APP_InternalFlashEraseSector(APP_FLASH_LOG_FATAL_INFO_SECTOR, sizeof(FatalLog_t));

    APP_InternalFlashWriteByte(APP_FLASH_LOG_FATAL_INFO_SECTOR, 
                                (uint8_t *)&SystemLog, 
                                sizeof(FatalLog_t)
                                );

#if defined(RELEASE)
	SystemAPP_Reset();
#else
    while(1) {
        /* Reset IWDG */
        SystemAPP_IWDGRefresh();

        /* FATAL debug option */
        Keyw = SystemAPP_ShellGetChar();

        SYS_PRINT("\n[FATAL]\n");
        
        switch (Keyw) {
        /* System reset */
        case 'r': {
            SystemAPP_Reset();
        }
        break;

        case 'l': {
            SYS_PRINT("\n[FATAL LOG]\n");
            SYS_PRINT("[Times] Fatal: %d\n",		SystemLog.FatalTimes);
            SYS_PRINT("[Times] Restart: %d\n\n",	SystemLog.RestartTimes);

            SYS_PRINT("[Fatal] Type: %s\n",		SystemLog.String);
            SYS_PRINT("[Fatal] Code: 0x%02X\n",	SystemLog.Code);

            SYS_PRINT("\n");
            SYS_PRINT("[Task] Id: %d\n",		SystemLog.CurrentTask.id);
            SYS_PRINT("[Task] Pri: %d\n",		SystemLog.CurrentTask.pri);
            SYS_PRINT("[Task] Entry: 0x%x\n",	SystemLog.CurrentTask.task);

            SYS_PRINT("\n");
            SYS_PRINT("[Obj] Task: %d\n",		SystemLog.CurrentActiveObject.des_task_id);
            SYS_PRINT("[Obj] Sig: %d\n",		SystemLog.CurrentActiveObject.sig);
            SYS_PRINT("[Obj] Type: 0x%x\n",		get_msg_type(&(SystemLog.CurrentActiveObject)));
            SYS_PRINT("[Obj] Ref count: %d\n",	get_msg_ref_count(&(SystemLog.CurrentActiveObject)));

            SYS_PRINT("\n");
            SYS_PRINT("[Core] IPSR: %d\n",			SystemLog.Core_Reg.IPSR);
            SYS_PRINT("[Core] PRIMASK: 0x%08X\n",	SystemLog.Core_Reg.PRIMASK);
            SYS_PRINT("[Core] CONTROL: 0x%08X\n",	SystemLog.Core_Reg.CONTROL_REG);
        }
        break;

        default:
            break;
        }

        /* LED notify FATAL */
        LifeLED_On();
        DELAY_TICK_Us( 65535 );
        LifeLED_Off();
        DELAY_TICK_Us( 65535 );
    }
#endif
}
