#ifndef __SYS_CTRL_H__
#define __SYS_CTRL_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "startup_code.h"


/********************************
 * @brief: FUNCTION PROTOTYPES  *
 ********************************/
extern void SystemAPP_Reset();

extern void SystemAPP_IWDGConfig();
extern void SystemAPP_IWDGRefresh();

extern void DELAY_TICK_Ms(volatile uint32_t time_out_ms);
extern void DELAY_TICK_Us(volatile uint32_t time_out_us);

extern uint32_t MILLIS(void);
extern uint32_t MICROS(void);

extern uint8_t  SystemAPP_IRQShellGetChar(void);
extern void     SystemAPP_IRQShellPutChar(uint8_t );
extern void     SystemAPP_ShellPutChar(uint8_t);
extern uint8_t  SystemAPP_ShellGetChar(void);

extern uint32_t SystemAPP_GetExceptionNumber(void);

extern void SystemAPP_PollingButton(void);

extern uint32_t SystemAPP_CRC_CalcCRC(uint32_t __num);

extern void     APP_InternalFlashEraseSector(uint32_t __addr, uint32_t __len);
extern uint8_t  APP_InternalFlashWriteByte(uint32_t __addr, uint8_t *__data, uint32_t __len);
extern void     APP_InternalFlashCopyDataTo(uint32_t __addr, void *__buf, uint32_t __len);
extern uint32_t APP_GetFlashSectorNumber(uint32_t __addr);


#ifdef __cplusplus
}
#endif

#endif // __SYS_CTRL_H__
