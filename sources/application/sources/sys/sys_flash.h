#ifndef __SYS_FLASH_H__
#define __SYS_FLASH_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "sys_dbg.h"

/**********************************************
 * @define: BASE ADDRESS OF THE FLASH SECTORS *
 ***********************************************/
#define NEXT_SECTOR_STEP_UNIT   ( 8 )

#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base address of Sector 0, 16 Kbytes   */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base address of Sector 1, 16 Kbytes   */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base address of Sector 2, 16 Kbytes   */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base address of Sector 3, 16 Kbytes   */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base address of Sector 4, 64 Kbytes   */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base address of Sector 5, 128 Kbytes  */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base address of Sector 6, 128 Kbytes  */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base address of Sector 7, 128 Kbytes  */
#define ADDR_FLASH_SECTOR_END   ((uint32_t)0x08080000) /* THE-END */


/*******************************************
 * @define: FLASH INTERNAL GENERAL DEFINES *
 *******************************************/
#define APP_FLASH_LOG_FATAL_INFO_SECTOR             ( ADDR_FLASH_SECTOR_7 )





#ifdef __cplusplus
}
#endif

#endif /* __SYS_FLASH_H__ */