CFLAGS		+= -Isources/app
CPPFLAGS	+= -Isources/app

APP_SOURCE_PATH = sources/app

# CPP source files
CPP_SOURCES += $(APP_SOURCE_PATH)/app.cpp
CPP_SOURCES += $(APP_SOURCE_PATH)/task_list.cpp
CPP_SOURCES += $(APP_SOURCE_PATH)/task_system.cpp
CPP_SOURCES += $(APP_SOURCE_PATH)/task_shell.cpp
CPP_SOURCES += $(APP_SOURCE_PATH)/task_life.cpp
CPP_SOURCES += $(APP_SOURCE_PATH)/task_firmware.cpp
CPP_SOURCES += $(APP_SOURCE_PATH)/app_bsp.cpp

