#include "app.h"
#include "app_bsp.h"
#include "app_dbg.h"

#include "sys_dbg.h"

#include "ak.h"
#include "task.h"
#include "message.h"

#include "task_list.h"

/************************************
 * @brief EXTERN & GLOBAL VARIABLES *
 ************************************/
button_t UserButton;


void Button_Callback(void* b) {
	button_t* Button_Obj = (button_t*)b;
	switch (Button_Obj->state) {
	case BUTTON_SW_STATE_PRESSED: {
        APP_DBG("[Button_Callback] BUTTON_SW_STATE_PRESSED\n");
	}
		break;

	case BUTTON_SW_STATE_LONG_PRESSED: {
        APP_DBG("[Button_Callback] BUTTON_SW_STATE_LONG_PRESSED\n");
	}
		break;

	case BUTTON_SW_STATE_RELEASED: {
        APP_DBG("[Button_Callback] BUTTON_SW_STATE_RELEASED\n");
	}
		break;

	default:
		break;
	}
}

