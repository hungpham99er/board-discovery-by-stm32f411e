#include <malloc.h>
#include <stdlib.h>
#include <string.h>

/* kernel include */
#include "ak.h"
#include "message.h"
#include "timer.h"
#include "fsm.h"

/* common include */
#include "ring_buffer.h"
#include "xprintf.h"
#include "log_queue.h"

/* driver include */
#include "led.h"
#include "button.h"

/* app include */
#include "app.h"
#include "app_dbg.h"
#include "app_bsp.h"

#include "task_list.h"
#include "task_shell.h"
#include "task_life.h"
#include "task_firmware.h"

#include "platform.h"
#include "io_cfg.h"

/* sys include */
#include "sys_ctrl.h"
#include "sys_dbg.h"

using namespace std;

/************************************
 * @brief EXTERN & GLOBAL VARIABLES *
 ************************************/
extern led_t LED_Life;
extern button_t UserButton;

extern SystemShell_t SysShell;

extern ring_buffer_char_t SerialBufferRead;
extern uint8_t BufferSerial[];

/****************************************
 * @brief: PRIVATE FUNCTION PROTOTYPES  *
 ****************************************/
static void app_start_timer();
static void app_init_state_machine();
static void app_task_init();

int main_app() {
    /************************************************************
    * Init active kernel
    **************************************************************/
    ENTRY_CRITICAL();
    task_init();
    task_create((task_t*)app_task_table);
    task_polling_create((task_polling_t*)app_task_polling_table);
    EXIT_CRITICAL();

    /************************************************************
    * Init applications
    **************************************************************/
    ENTRY_CRITICAL();

    /**********************
    * Software configure
    ************************/
    /* Init watch dog timer time out 20s */
    SystemAPP_IWDGConfig();

   /**********************
    * Hardware configure
    ************************/
    /* LED life init */
    led_init(&LED_Life, LifeLED_Config, LifeLED_On, LifeLED_Off);

    /* Button init */
	button_init(&UserButton, 10, USER_BUTTON_ID, UserButton_Config,	UserButton_Reading, Button_Callback);
	button_enable(&UserButton);

    /* Start timer for application */
    app_init_state_machine();
    app_start_timer();

    /************************************************************
    * App task initial
    **************************************************************/
    app_task_init();

    EXIT_CRITICAL();
    
    /************************************************************
    * Run applications
    **************************************************************/
    return task_run();
}

/**
 * @brief 
 * 
 */
void app_start_timer() {
    /* Start timer to toggle life led */
    timer_set(AC_TASK_LIFE_ID, AC_LIFE_SYSTEM_CHECK, AC_LIFE_TASK_TIMER_LED_LIFE_INTERVAL, TIMER_PERIODIC);
}

/**
 * @brief 
 * 
 */
void app_init_state_machine() {

}

/**
 * @brief 
 * 
 */
void app_task_init() {
    task_post_pure_msg(AC_TASK_SYSTEM_ID, AC_SYSTEM_CHECK_FIRMWARE);
    task_post_pure_msg(AC_TASK_SYSTEM_ID, AC_SYSTEM_FLASH_WRITE);
}

/**
 * @brief 
 * 
 */
void AppPollingButton() {
    button_timer_polling(&UserButton);
}

/**
 * @brief 
 * 
 * @param Byte 
 */
void AppPollingShell(uint8_t Byte) {
    if (SysShell.iDxData < SHELL_BUFFER_LENGHT - 1) {
        if (Byte == '\r' || Byte == '\n') {
            xputc('\r');
            xputc('\n');

            SysShell.Data[SysShell.iDxData] = Byte;

            task_post_common_msg(AC_TASK_SHELL_ID, 
                                AC_SHELL_HANDLE_COMMAND, 
                                (uint8_t*)&SysShell.Data[0], 
                                SysShell.iDxData + 1
                                );

            SysShell.iDxData = 0;
        }
        else {
            xputc(Byte);
            if (Byte == 8 && SysShell.iDxData > 0) { /* Backspace */
                --(SysShell.iDxData);
            }
            else {
                SysShell.Data[SysShell.iDxData++] = Byte;
            }
        }
    }
}
