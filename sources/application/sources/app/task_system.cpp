#include "fsm.h"
#include "port.h"
#include "message.h"
#include "timer.h"
#include "heap.h"

#include "sys_ctrl.h"
#include "sys_dbg.h"
#include "sys_flash.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_system.h"


void task_system(ak_msg_t* msg) {
    switch (msg->sig) {
    case AC_SYSTEM_CHECK_FIRMWARE: {
        /**
         * @brief Linker script
         **/
        extern uint32_t _start_flash;
        extern uint32_t _end_flash;
        extern uint32_t _sdata;
        extern uint32_t _edata;

        uint32_t Len_Of_FLASH = ( (uint32_t)&_end_flash - (uint32_t)&_start_flash ) + \
                            ( (uint32_t)&_edata - (uint32_t)&_sdata );

        APP_PRINT("Length of FLASH: %ld\n", Len_Of_FLASH);

        uint32_t Sum_Calc = 0;
        for (uint32_t _Addr_ = (uint32_t)&_start_flash; _Addr_ < ( (uint32_t)&_start_flash + Len_Of_FLASH ); _Addr_ += sizeof(uint32_t)) {
            Sum_Calc += *((uint32_t *)_Addr_);
        }

        uint32_t Checksum_Calc = SystemAPP_CRC_CalcCRC( Sum_Calc );
        APP_PRINT("Checksum of FIRMWARE: %X\n", Checksum_Calc);
    }
    break;

    case AC_SYSTEM_FLASH_WRITE: {

    }
    break;

    case AC_SYSTEM_FLASH_CLEAR: {
        
    }
    break;

    default:
	break;
    }
}
