#include "task_list.h"
#include "timer.h"

#include "app.h"


/************************
 * @var: app_task_table *
 ************************/
const task_t app_task_table[] = {
	/***********************************************************************
								SYSTEM TASK 							 
	*************************************************************************/
    {	TASK_TIMER_TICK_ID		,	TASK_PRI_LEVEL_7,       task_timer_tick		},

	/***********************************************************************
								APP TASK 							 
	*************************************************************************/
    {	AC_TASK_SYSTEM_ID		,	TASK_PRI_LEVEL_2	,	task_system			},
    {	AC_TASK_SHELL_ID		,	TASK_PRI_LEVEL_2	,	task_shell			},
    {	AC_TASK_LIFE_ID			,	TASK_PRI_LEVEL_6	,	task_life			},
	{	AC_TASK_FIRMWARE_ID		,	TASK_PRI_LEVEL_5	,	task_firmware		},

	/***********************************************************************
								END OF TABLE 							 
	*************************************************************************/
	{	AK_TASK_EOT_ID				,	TASK_PRI_LEVEL_0	,	(pf_task)0		}
};

/********************************
 * @var: app_task_polling_table *
 ********************************/
const task_polling_t app_task_polling_table[] = {
	/***********************************************************************
								APP TASK 							 
	************************************************************************/

	/***********************************************************************
								END OF TABLE							 
	*************************************************************************/
	{	AK_TASK_POLLING_EOT_ID		,	AK_DISABLE	,	(pf_task_polling)0		},
};
