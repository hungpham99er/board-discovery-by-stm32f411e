#include "fsm.h"
#include "port.h"
#include "message.h"

#include "sys_ctrl.h"
#include "sys_dbg.h"

#include "io_cfg.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_firmware.h"


/************************************
 * @brief EXTERN & GLOBAL VARIABLES *
 ************************************/
struct FIRMWARE_HEADER FirmwareHeader;


/****************************************
 * @brief: PRIVATE FUNCTION PROTOTYPES  *
 ****************************************/


void task_firmware(ak_msg_t* msg) {
	switch (msg->sig) {
	case AC_FIRMWARE_UPDATE_INFO_REQ: {
        APP_DBG("[AC_TASK_FIRMWARE_ID] AC_FIRMWARE_UPDATE_REQ\n");

		/************************
         * @brief Linker script *
         ************************/
		extern uint32_t _start_flash;
        extern uint32_t _end_flash;
        extern uint32_t _sdata;
        extern uint32_t _edata;

		ENTRY_CRITICAL(); 
		FirmwareHeader.FwCurrentLenght = ( (uint32_t)&_end_flash - (uint32_t)&_start_flash ) + \
                            			( (uint32_t)&_edata - (uint32_t)&_sdata );

		for (uint32_t _addr = (uint32_t)&_start_flash; 							\
			_addr < (uint32_t)&_start_flash + FirmwareHeader.FwCurrentLenght;	\
			_addr += sizeof(uint32_t))
		{
			FirmwareHeader.FwCurrentChecksum += *((uint32_t *)_addr);
		}
		FirmwareHeader.FwCurrentChecksum &= ( 0xFFFF );

		EXIT_CRITICAL();

		APP_PRINT("[FIRMWARE INFORMATION]\n");
		APP_PRINT("\t[CURRENT] Firmware length:   %d\n", FirmwareHeader.FwCurrentLenght);
		APP_PRINT("\t[CURRENT] Firmware checksum: %d\n", FirmwareHeader.FwCurrentChecksum);

		task_post_pure_msg(AC_TASK_FIRMWARE_ID, AC_FIRMWARE_UPDATE_INFO_RES);
	}
	break;

	case AC_FIRMWARE_UPDATE_INFO_RES: {
		APP_DBG("[AC_TASK_FIRMWARE_ID] AC_FIRMWARE_UPDATE_INFO_RES\n");


	}
	break;

	case AC_FIRMWARE_TRANSFER_DATA_REQ: {
		APP_DBG("[AC_TASK_FIRMWARE_ID] AC_FIRMWARE_TRANSFER_DATA_REQ\n");
	}
	break;

	case AC_FIRMWARE_TRANSFER_DATA_RES: {
		APP_DBG("[AC_TASK_FIRMWARE_ID] AC_FIRMWARE_TRANSFER_DATA_RES\n");

	}
	break;

	default:
		break;
	}
}
