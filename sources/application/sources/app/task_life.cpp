#include "fsm.h"
#include "port.h"
#include "message.h"

#include "sys_ctrl.h"
#include "sys_dbg.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_life.h"


/************************************
 * @brief EXTERN & GLOBAL VARIABLES *
 ************************************/
led_t LED_Life;

void task_life(ak_msg_t* msg) {
	switch (msg->sig) {
	case AC_LIFE_SYSTEM_CHECK: {
        APP_DBG("[AC_TASK_LIFE_ID] AC_LIFE_SYSTEM_CHECK\n");

	    /* Reset IWDG */
	    SystemAPP_IWDGRefresh();

	    /* Toggle led life to indicator */
	    led_toggle(&LED_Life);
	}
	break;

	default:
		break;
	}
}
