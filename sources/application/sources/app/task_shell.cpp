#include <stdbool.h>

#include "fsm.h"
#include "port.h"
#include "message.h"

#include "cmd_line.h"
#include "xprintf.h"

#include "sys_dbg.h"
#include "sys_ctrl.h"
#include "sys_flash.h"

#include "io_cfg.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"
#include "task_shell.h"


/************************************
 * @brief EXTERN & GLOBAL VARIABLES *
 ************************************/
SystemShell_t SysShell;


/****************************************
 * @brief: PRIVATE FUNCTION PROTOTYPES  *
 ****************************************/
static int8_t prvShellRebootRequest(uint8_t *argv);
static int8_t prvShellHelpRequest(uint8_t *argv);
static int8_t prvShellFatalRequest(uint8_t *argv);

/****************************
 * @var: COMMAND LINE TABLE *
 ****************************/
static struct COMMAND_LINE_TABLE TableCommandLine[] = {
	{	(const char *)"reboot", prvShellRebootRequest,	(const char *)"System reboot"	},
	{	(const char *)"fatal", 	prvShellFatalRequest,	(const char *)"Fatal log"		},
	{	(const char *)"help",	prvShellHelpRequest,	(const char *)"Help"			},
};
static uint8_t s_CmdTblLength = sizeof(TableCommandLine)/sizeof(TableCommandLine[0]);


void task_shell(ak_msg_t* msg) {
	switch (msg->sig) {
		case AC_SHELL_HANDLE_COMMAND: {
			APP_DBG ("[AC_TASK_SHELL_ID] AC_SHELL_HANDLE_COMMAND\n");

			uint8_t *Cmd = get_data_common_msg(msg);
			uint8_t Keyw[10];

			for (uint8_t iDxKeyw = 0; iDxKeyw < get_data_len_common_msg(msg); ++iDxKeyw) {
				if (Cmd[iDxKeyw] == ' ' || Cmd[iDxKeyw] == '\r' || Cmd[iDxKeyw] == '\n') {
					Keyw[iDxKeyw] = 0;

					break; 
				}
				Keyw[iDxKeyw] = Cmd[iDxKeyw];	
			}

			for (uint8_t iDx = 0; iDx < s_CmdTblLength; ++iDx) {
				if (strcmp((const char *)Keyw, TableCommandLine[iDx].Cmnd) == 0) {
					TableCommandLine[iDx].Func(Cmd);

					break;
				}
			}
		}
		break;

		default: {
			FATAL("SHELL", 0x01);
		}
		break;
	}

    APP_PRINT("> ");
}

int8_t prvShellRebootRequest(uint8_t *argv) {
	APP_DBG ("[AC_TASK_SHELL_ID] SHELL_REBOOT_REQUEST\n");
	SystemAPP_Reset();

	return 0;
}

int8_t prvShellHelpRequest(uint8_t *argv) {
	APP_DBG ("[AC_TASK_SHELL_ID] SHELL_HELP_REQUEST\n");

	struct COMMAND_LINE_TABLE *pxCmdTbl = TableCommandLine;

	APP_PRINT("[System help]\n");
	for (uint8_t iDxCmdTbl = 0; iDxCmdTbl < s_CmdTblLength; ++iDxCmdTbl) {
		APP_PRINT("\t%s...........%s\n", pxCmdTbl[iDxCmdTbl].Cmnd,
										pxCmdTbl[iDxCmdTbl].Info
										);
	}

    return 0;
}

int8_t prvShellFatalRequest(uint8_t *argv) {
	APP_DBG ("[AC_TASK_SHELL_ID] SHELL_FATAL_REQUEST\n");

	FatalLog_t SystemLog;

	switch ( *(argv + 6) ) {
	case 'l': {
		FatalLog_t SystemLog;
		APP_InternalFlashCopyDataTo(APP_FLASH_LOG_FATAL_INFO_SECTOR,
									&SystemLog, 
									sizeof(FatalLog_t)
									);

		SYS_PRINT("\n[FATAL LOG]\n");
		SYS_PRINT("[Times] Fatal: %d\n",		SystemLog.FatalTimes);
		SYS_PRINT("[Times] Restart: %d\n\n",	SystemLog.RestartTimes);

		SYS_PRINT("[Fatal] Type: %s\n",		SystemLog.String);
		SYS_PRINT("[Fatal] Code: 0x%02X\n",	SystemLog.Code);

		SYS_PRINT("\n");
		SYS_PRINT("[Task] Id: %d\n",		SystemLog.CurrentTask.id);
		SYS_PRINT("[Task] Pri: %d\n",		SystemLog.CurrentTask.pri);
		SYS_PRINT("[Task] Entry: 0x%x\n",	SystemLog.CurrentTask.task);

		SYS_PRINT("\n");
		SYS_PRINT("[Obj] Task: %d\n",		SystemLog.CurrentActiveObject.des_task_id);
		SYS_PRINT("[Obj] Sig: %d\n",		SystemLog.CurrentActiveObject.sig);
		SYS_PRINT("[Obj] Type: 0x%x\n",		get_msg_type(&(SystemLog.CurrentActiveObject)));
		SYS_PRINT("[Obj] Ref count: %d\n",	get_msg_ref_count(&(SystemLog.CurrentActiveObject)));

		SYS_PRINT("\n");
		SYS_PRINT("[Core] IPSR: %d\n",			SystemLog.Core_Reg.IPSR);
		SYS_PRINT("[Core] PRIMASK: 0x%08X\n",	SystemLog.Core_Reg.PRIMASK);
		SYS_PRINT("[Core] CONTROL: 0x%08X\n",	SystemLog.Core_Reg.CONTROL_REG);
	}
	break;

	case 'r': {
		memset(&SystemLog, 0, sizeof(FatalLog_t));
		APP_InternalFlashEraseSector(APP_FLASH_LOG_FATAL_INFO_SECTOR, sizeof(FatalLog_t));
		APP_InternalFlashWriteByte(APP_FLASH_LOG_FATAL_INFO_SECTOR, 
                                (uint8_t *)&SystemLog, 
                                sizeof(FatalLog_t)
                                );
		
		APP_PRINT("RESET FATAL-LOG [OK]\n");
	}
	break;

	case 't': {
		FATAL("TEST", 0x01);
	}
	break;	

	default:
		break;
	}

	return 0;
}
