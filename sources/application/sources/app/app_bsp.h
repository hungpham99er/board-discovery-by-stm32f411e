#ifndef __APP_BSP_H__
#define __APP_BSP_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "button.h"

/*********************************
 * @define: USER BUTTON DEFINES  *
 *********************************/
#define USER_BUTTON_ID					(0x01)

/************************************
 * @brief EXTERN & GLOBAL VARIABLES *
 ************************************/
extern button_t UserButton;

/********************************
 * @brief: FUNCTION PROTOTYPES  *
 ********************************/
extern void Button_Callback(void*);

#ifdef __cplusplus
}
#endif

#endif //__APP_BSP_H__
