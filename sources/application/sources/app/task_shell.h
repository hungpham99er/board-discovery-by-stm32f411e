#ifndef __TASK_SHELL_H__
#define __TASK_SHELL_H__

#include "ring_buffer.h"

/***************************************
 * @define: TASK SHELL GENERAL DEFINES *
 ***************************************/
#define SHELL_BUFFER_LENGHT					(32U)
#define BUFFER_CONSOLE_REV_SIZE				(32U)

typedef struct SYSTEM_SHELL {
	uint8_t iDxData;
	uint8_t Data[SHELL_BUFFER_LENGHT];
} SystemShell_t;

typedef int8_t (*pf_CmndFunc)(uint8_t* argv);

struct COMMAND_LINE_TABLE {
	const char *Cmnd;
	pf_CmndFunc Func;
	const char *Info;
};

/************************************
 * @brief EXTERN & GLOBAL VARIABLES *
 ************************************/
extern SystemShell_t SysShell;

#endif /* __TASK_SHELL_H__ */
