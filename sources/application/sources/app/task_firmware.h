#ifndef __TASK_FIRMWARE_H__
#define __TASK_FIRMWARE_H__

#include <stdint.h>

#include "ring_buffer.h"

/******************************************
 * @define: TASK FIRMWARE GENERAL DEFINES *
 ******************************************/
#define SERIAL_DATA_TRANSFER_MAXIMUM    ( 64 )
#define SERIAL_BUFFER_READ_SIZE         ( 128 )

struct FIRMWARE_HEADER {
    /********************
     * Firmware current *
     ********************/
    uint32_t FwCurrentLenght;
    uint32_t FwCurrentChecksum;

    /****************
     * Firmware new *
     ****************/
    uint32_t FwNewLenght;
    uint32_t FwNewChecksum;
};

/************************************
 * @brief EXTERN & GLOBAL VARIABLES *
 ************************************/
extern struct FIRMWARE_HEADER FirmwareHeader;


#endif /* __TASK_FIRMWARE_H__ */
