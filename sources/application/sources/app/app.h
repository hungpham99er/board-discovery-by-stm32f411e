/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   13/08/2016
 ******************************************************************************
**/

#ifndef __APP_H__
#define __APP_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "ak.h"


/*****************************************************************************/
/* SYSTEM task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */


/*****************************************************************************/
/*  LIFE task define
 */
/*****************************************************************************/
/* define timer */
#define AC_LIFE_TASK_TIMER_LED_LIFE_INTERVAL		(1000)

/* define signal */
enum {
    AC_LIFE_SYSTEM_CHECK = AK_USER_DEFINE_SIG,
};

/*****************************************************************************/
/*  SHELL task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
    AC_SHELL_HANDLE_COMMAND = AK_USER_DEFINE_SIG,
};

/*****************************************************************************/
/*  SYSTEM task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
    AC_SYSTEM_CHECK_FIRMWARE = AK_USER_DEFINE_SIG,
    AC_SYSTEM_FLASH_WRITE,
    AC_SYSTEM_FLASH_CLEAR,
};


/*****************************************************************************/
/*  MODBUS task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */


/*****************************************************************************/
/*  FIRMWARE task define
 */
/*****************************************************************************/
/* define timer */
/* define signal */
enum {
    AC_FIRMWARE_UPDATE_INFO_REQ = AK_USER_DEFINE_SIG,
    AC_FIRMWARE_UPDATE_INFO_RES,
    AC_FIRMWARE_TRANSFER_DATA_REQ,
    AC_FIRMWARE_TRANSFER_DATA_RES,
};


/*****************************************************************************/
/*  global define variable
 */
/*****************************************************************************/



extern int main_app(void);
extern void AppPollingShell(uint8_t Byte);
extern void AppPollingButton(void);

#ifdef __cplusplus
}
#endif

#endif //__APP_H__
