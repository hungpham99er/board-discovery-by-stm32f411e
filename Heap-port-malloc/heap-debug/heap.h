/**
 * @file heap.h
 * @author Hun9
 * @brief Porting Dynamic Allocation
 * @date 29/03/2022
 */

#ifndef __HEAP_H__
#define __HEAP_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <stddef.h>

#include "port.h"
#include "sys_dbg.h"


/*=====================
 *    Linker script 
 ======================*/
extern uint32_t __heap_start__;
extern uint32_t __heap_end__;


/*========================================================================
 * #### Heap defines ####
 =========================================================================*/
#define TOTAL_HEAP_SIZE                 ( 3072U )

#define HEAP_START_ADDR                 ( &__heap_start__ )
#define HEAP_END_ADDR                   ( &__heap_end__   )

#define BLOCK_LINK_STRUCT_SIZE          ( sizeof(struct BLOCK_LINK) )

/* FATAL heap */
#define INVALID_VALUE_ALLOCATED()       FATAL("HEAP", 0x01)
#define INSUFFICENT_HEAP_MEMORY()       FATAL("HEAP", 0x02)
#define HEAP_OVERFLOW()                 FATAL("HEAP", 0x03)
#define INVALID_VALUE_FREE()            FATAL("HEAP", 0x04)
#define INVALID_BLOCK_TO_FREE()         FATAL("HEAP", 0x05)


#ifndef ENTRY_CRITICAL
#define ENTRY_CRITICAL()                __enable_irq()
#define EXIT_CRITICAL()                 __disable_irq()
#endif

/**
 * =====================
 * @struct: HEAP_REGION
 * @brief: Store heap's informations
 */
typedef struct HEAP_REGION {
    uint8_t  *pxHeap_Start_Address;
    uint32_t Total_Size;
    uint32_t Free_Size;
    uint32_t Used_Size;
} HeapRegion_t;

/**
 * ======================
 * @struct: HEAP_BLOCK
 * @brief: Linker list BLOCK
 */
typedef struct BLOCK_LINK {
    struct BLOCK_LINK *pNext_FreeBLOCK;
    uint32_t SizeOfBLOCK;     
} BlockLink_t;

/*========================================================================
 * #### Prototype functions ####
 =========================================================================*/
extern void*    PortMalloc(uint32_t Num_Of_Byte);
extern void     PortFree(void *pxFree);
extern uint32_t Get_TotalHeapSize(void);
extern uint32_t Get_TotalHeapFree(void);
extern uint32_t Get_TotalHeapUsed(void);
extern uint32_t Get_MaxFreeBLOCKSize(void);
extern uint32_t Get_MinFreeBLOCKSize(void);


/*========================================================================
 * #### Debug heap defines ####
 =========================================================================*/
#define HEAP_DEBUG_ENABLE               ( 1 )
#define DEBUG_BLOCK_TOTAL               ( 32U )

typedef enum {
    FREE = 0,
    ALLOCATED,
} StatusBLOCK_t;

/**
 * ====================
 * @struct: DEBUG_BLOCK
 */
typedef struct DEBUG_BLOCK {
    uint32_t       Address_BLOCK;
    uint32_t       DataAddress_BLOCK;
    uint32_t       SizeOfBLOCK;
    StatusBLOCK_t  Status_BLOCK;
    uint8_t        IndexOfBLOCK;
} DebugBLOCK_t;

/**
 * ========================
 * @struct: BLOCK_CONTAINER
 */
typedef struct BLOCK_CONTAINER {
    DebugBLOCK_t BLOCKS[DEBUG_BLOCK_TOTAL];
    uint8_t      Idx;
} BLOCKContainer_t;

/*========================================================================
 * #### Prototype functions ####
 =========================================================================*/
extern void DEBUG_ListFreeBLOCKTracking(void);
extern void DEBUG_AllocatedBLOCKTracking(void);
extern void DEBUG_HeapTracking(void);



#ifdef __cplusplus
}
#endif

#endif // __AK_HEAP_H__
