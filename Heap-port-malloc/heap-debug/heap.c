#include <stdlib.h>
#include <string.h>

#include "heap.h"

#include "platform.h"


/*========================================================================
 * #### Private variables ####
 =========================================================================*/
static HeapRegion_t HeapRegionObject = {
    ( (uint8_t *)HEAP_START_ADDR ),
    ( TOTAL_HEAP_SIZE ),
    ( TOTAL_HEAP_SIZE - BLOCK_LINK_STRUCT_SIZE ),
    ( (uint32_t)0U )
};

static BlockLink_t  StartBLOCK;
static BlockLink_t  *pEndBLOCK = NULL;

static BLOCKContainer_t DbgBLOCK;

/*========================================================================
 * #### Private function prototypes ####
 =========================================================================*/
static void prvHeap_Init( void );
static void prvHeap_ExpandFreeBLOCK(BlockLink_t *pxExpand_BLOCK);
static void prvLink_BLOCK(BlockLink_t *pxLink_BLOCK);
static void prvLink_FromStartBLOCK(BlockLink_t *pxStart_BLOCK, BlockLink_t *pxInsert_BLOCK);
static void prvLink_FromInsertBLOCK(BlockLink_t *pxInsert_BLOCK, BlockLink_t *pxStart_BLOCK);

static int prvBLOCKCompare (const DebugBLOCK_t *BLOCK_A, const DebugBLOCK_t *BLOCK_B);

/*========================================================================
 * #### Heap functions implement ####
 =========================================================================*/
/**
 * ======================
 * @function: PortMalloc
 */
void * PortMalloc(uint32_t Num_Of_Byte) {
    BlockLink_t * pxTraverse_BLOCK, * pxPrev_BLOCK, * pxExpand_BLOCK;
    void * pvReturn = NULL;
    uint32_t Total_Byte_Allocated = 0U;

    // Init heap when akMalloc called at the first time
    if(pEndBLOCK == NULL) {
        prvHeap_Init();

#if defined(HEAP_DEBUG_ENABLE)
        memset(&DbgBLOCK, 0, sizeof(DbgBLOCK));
        uint8_t idx  = 0;
        for (; idx < DEBUG_BLOCK_TOTAL; idx++) {
            DbgBLOCK.BLOCKS[idx].IndexOfBLOCK = 0xFFU;
        }
#endif
    }

    ENTRY_CRITICAL();

    // FATAL if Num_Of_Byte is invalid
    if (Num_Of_Byte < 0) {
        INVALID_VALUE_ALLOCATED();
    }
    
    // FATAL if heap is overflow
    if (HeapRegionObject.Free_Size < Total_Byte_Allocated) {
        HEAP_OVERFLOW();
    }

    Total_Byte_Allocated = Num_Of_Byte + BLOCK_LINK_STRUCT_SIZE;

    // Start BLOCK to traverse
    pxPrev_BLOCK = &StartBLOCK;
    pxTraverse_BLOCK = StartBLOCK.pNext_FreeBLOCK;    

    // Traverse until meeting sufficient BLOCK
    while ((pxTraverse_BLOCK->SizeOfBLOCK < Total_Byte_Allocated) && (NULL != pxTraverse_BLOCK->pNext_FreeBLOCK)) {
        pxPrev_BLOCK = pxTraverse_BLOCK;
        pxTraverse_BLOCK = pxTraverse_BLOCK->pNext_FreeBLOCK;
    }

    // FATAL if not find sufficient BLOCK in heap
    if (pxTraverse_BLOCK == pEndBLOCK) {
        INSUFFICENT_HEAP_MEMORY();
    }

    pvReturn = (void *)((uint8_t *)pxPrev_BLOCK->pNext_FreeBLOCK + BLOCK_LINK_STRUCT_SIZE);
    pxPrev_BLOCK->pNext_FreeBLOCK = pxTraverse_BLOCK->pNext_FreeBLOCK;

    // TRACKING DEBUG =======================================
#if defined(HEAP_DEBUG_ENABLE)
    while (ALLOCATED == DbgBLOCK.BLOCKS[DbgBLOCK.Idx].Status_BLOCK) {
        ++(DbgBLOCK.Idx);
    }

    if (0xFFU == DbgBLOCK.BLOCKS[DbgBLOCK.Idx].IndexOfBLOCK) {
        DbgBLOCK.BLOCKS[DbgBLOCK.Idx].IndexOfBLOCK = DbgBLOCK.Idx;
    }
    
    DbgBLOCK.BLOCKS[DbgBLOCK.Idx].DataAddress_BLOCK = (uint32_t)pvReturn;
    DbgBLOCK.BLOCKS[DbgBLOCK.Idx].Address_BLOCK = (uint32_t)pvReturn - BLOCK_LINK_STRUCT_SIZE;
    DbgBLOCK.BLOCKS[DbgBLOCK.Idx].SizeOfBLOCK = Total_Byte_Allocated;
    DbgBLOCK.BLOCKS[DbgBLOCK.Idx].Status_BLOCK = ALLOCATED;
#endif

    // Split free BLOCK into two if size of memory allocation is too large
    if( (pxTraverse_BLOCK->SizeOfBLOCK - Total_Byte_Allocated ) > (BLOCK_LINK_STRUCT_SIZE * 2U)) {
        // Point to address after BLOCK allocated
        pxExpand_BLOCK = (struct BLOCK_LINK *)((uint8_t *)pxTraverse_BLOCK + Total_Byte_Allocated);
        pxExpand_BLOCK->SizeOfBLOCK = pxTraverse_BLOCK->SizeOfBLOCK - Total_Byte_Allocated;

        pxTraverse_BLOCK->SizeOfBLOCK = Total_Byte_Allocated;

        // Expand free BLOCK list
        prvHeap_ExpandFreeBLOCK(pxExpand_BLOCK);
    }

    pxTraverse_BLOCK->pNext_FreeBLOCK = NULL;

    // Update heap info
    HeapRegionObject.Free_Size -= Total_Byte_Allocated;
    HeapRegionObject.Used_Size += Total_Byte_Allocated;

    EXIT_CRITICAL();

    return pvReturn;
}

/**
 * ====================
 * @fucntion: PortFree
 */
void PortFree(void * pxFree) {
    BlockLink_t *pxFree_BLOCK;

    // FATAL if pxFree is invalid
    if (NULL == pxFree) {
        INVALID_VALUE_FREE();
    }

    // Point to BLOCK which needs to free
    pxFree_BLOCK = (struct BLOCK_LINK *)((uint8_t *)pxFree - BLOCK_LINK_STRUCT_SIZE);

    // FATAL if BLOCK to free is invalid
    if ((NULL != pxFree_BLOCK->pNext_FreeBLOCK) || (pxFree_BLOCK->SizeOfBLOCK <= 0U)) {
        INVALID_BLOCK_TO_FREE();
    }

    // Expand free BLOCK list
    prvLink_BLOCK(pxFree_BLOCK);

    // Update HeapRegionObject
    HeapRegionObject.Free_Size += pxFree_BLOCK->SizeOfBLOCK;
    HeapRegionObject.Used_Size -= pxFree_BLOCK->SizeOfBLOCK;

    // TRACKING DEBUG =======================================
#if defined(HEAP_DEBUG_ENABLE)
    uint8_t idx = 0;
    for (; idx < DEBUG_BLOCK_TOTAL; ++idx) {
        if ((DbgBLOCK.BLOCKS[idx].Address_BLOCK == (uint32_t)pxFree_BLOCK) && 
                (ALLOCATED == DbgBLOCK.BLOCKS[idx].Status_BLOCK)) 
        {
            memset(&DbgBLOCK.BLOCKS[idx], 0U, sizeof(DbgBLOCK.BLOCKS[idx]));
            DbgBLOCK.Idx = DbgBLOCK.BLOCKS[idx].IndexOfBLOCK;
            break;
        }
    }    
#endif

}

/**
 * ============================
 * @function: Get_TotalHeapFree
 */
uint32_t Get_TotalHeapFree() {
    uint32_t Free_Size;

    ENTRY_CRITICAL();
    Free_Size = HeapRegionObject.Free_Size;
    EXIT_CRITICAL();

    return Free_Size;
}

/**
 * =============================
 * @function: Get_TotalHeapSize
 */
uint32_t Get_TotalHeapSize() {
    uint32_t Total_Size;

    ENTRY_CRITICAL();
    Total_Size = HeapRegionObject.Total_Size;
    EXIT_CRITICAL();

    return Total_Size;
}

/**
 * ============================
 * @function: Get_TotalHeapUsed
 */
uint32_t Get_TotalHeapUsed() {
    uint32_t Used_Size;

    ENTRY_CRITICAL();
    Used_Size = HeapRegionObject.Used_Size;
    EXIT_CRITICAL();

    return Used_Size;
}

/**
 * ===============================
 * @function: Get_MaxFreeBLOCKSize
 */
uint32_t Get_MaxFreeBLOCKSize() {
    uint32_t MaxBLOCKSize = 0U;
    BlockLink_t *pxRun_BLOCK = StartBLOCK.pNext_FreeBLOCK;

    ENTRY_CRITICAL();
    while (pxRun_BLOCK != pEndBLOCK) {
        MaxBLOCKSize = (pxRun_BLOCK->SizeOfBLOCK > MaxBLOCKSize) ? (pxRun_BLOCK->SizeOfBLOCK) : MaxBLOCKSize;
        pxRun_BLOCK = pxRun_BLOCK->pNext_FreeBLOCK;
    }
    EXIT_CRITICAL();

    return MaxBLOCKSize;
}

/**
 * ===============================
 * @function: Get_MinFreeBLOCKSize
 */
uint32_t Get_MinFreeBLOCKSize() {
    uint32_t MinBLOCKSize = TOTAL_HEAP_SIZE;
    BlockLink_t *pxRun_BLOCK = StartBLOCK.pNext_FreeBLOCK;

    ENTRY_CRITICAL();
    while (pxRun_BLOCK != pEndBLOCK) {
        MinBLOCKSize = (pxRun_BLOCK->SizeOfBLOCK < MinBLOCKSize) ? (pxRun_BLOCK->SizeOfBLOCK) : MinBLOCKSize;
        pxRun_BLOCK = pxRun_BLOCK->pNext_FreeBLOCK;
    }
    EXIT_CRITICAL();

    return MinBLOCKSize;
}

/**
 * ======================
 * @function: prvHeapInit
 */
void prvHeap_Init() {
    BlockLink_t * pxInitFirst_BLOCK;

    // Setup StartBLOCK point to first heap's address
    StartBLOCK.pNext_FreeBLOCK = (struct BLOCK_LINK *)HeapRegionObject.pxHeap_Start_Address;
    StartBLOCK.SizeOfBLOCK = 0U;

    // Setup pEndBLOCK point to last BLOCK
    pEndBLOCK = (struct BLOCK_LINK *)(((uint32_t)HeapRegionObject.pxHeap_Start_Address + TOTAL_HEAP_SIZE) - BLOCK_LINK_STRUCT_SIZE);
    pEndBLOCK->SizeOfBLOCK = 0U;
    pEndBLOCK->pNext_FreeBLOCK = NULL;

    // Init first BLOCK
    pxInitFirst_BLOCK = (struct BLOCK_LINK *)HeapRegionObject.pxHeap_Start_Address;
    pxInitFirst_BLOCK->SizeOfBLOCK = (uint32_t)pEndBLOCK - ( uint32_t )pxInitFirst_BLOCK;
    pxInitFirst_BLOCK->pNext_FreeBLOCK = pEndBLOCK;
}


/**
 * ===================================
 * @function: prvHeap_ExpandFreeBLOCK
 */
void prvHeap_ExpandFreeBLOCK(BlockLink_t * pxExpand_BLOCK) {
    BlockLink_t *pxTraverse_BLOCK = &StartBLOCK;

    while ((uint8_t *)pxTraverse_BLOCK->pNext_FreeBLOCK < (uint8_t *)pxExpand_BLOCK ) {
        pxTraverse_BLOCK = pxTraverse_BLOCK->pNext_FreeBLOCK;
    }

    if (pxTraverse_BLOCK != pEndBLOCK) {
        pxExpand_BLOCK->pNext_FreeBLOCK = pxTraverse_BLOCK->pNext_FreeBLOCK;
        pxTraverse_BLOCK->pNext_FreeBLOCK = pxExpand_BLOCK;
    }
    else {
        pxExpand_BLOCK->pNext_FreeBLOCK = pEndBLOCK;
        pxTraverse_BLOCK->pNext_FreeBLOCK = pxExpand_BLOCK;
    }

    if (pxTraverse_BLOCK == &StartBLOCK) {
        pxTraverse_BLOCK->pNext_FreeBLOCK = pxExpand_BLOCK;
    }
}


/**
 * ========================
 * @function: prvLink_BLOCK
 */
void prvLink_BLOCK(BlockLink_t * pxLink_BLOCK) {
    BlockLink_t *pxTraverse_BLOCK = StartBLOCK.pNext_FreeBLOCK;

    if ((uint8_t *)pxTraverse_BLOCK < (uint8_t *)pxLink_BLOCK) {
        // Traversing until finding free BLOCK nearest BLOCK needs to link
        while ((uint8_t *)pxTraverse_BLOCK->pNext_FreeBLOCK < (uint8_t *)pxLink_BLOCK) {
            pxTraverse_BLOCK = pxTraverse_BLOCK->pNext_FreeBLOCK;
        }
        prvLink_FromStartBLOCK(pxTraverse_BLOCK, pxLink_BLOCK);
    }
    else if ((uint8_t *)pxTraverse_BLOCK > (uint8_t *)pxLink_BLOCK) {
        prvLink_FromInsertBLOCK(pxLink_BLOCK, pxTraverse_BLOCK);
    }
    else {
        // !! NOT occurs
    }
}


/**
 * =================================
 * @function: prvLink_FromStartBLOCK
 */
void prvLink_FromStartBLOCK(BlockLink_t *pxStart_BLOCK, BlockLink_t *pxInsert_BLOCK) {
    uint8_t *pxTemp = (uint8_t *)pxStart_BLOCK;

    if (pxTemp + pxStart_BLOCK->SizeOfBLOCK == (uint8_t *)pxInsert_BLOCK) {
        pxStart_BLOCK->SizeOfBLOCK += pxInsert_BLOCK->SizeOfBLOCK;
        pxInsert_BLOCK = pxStart_BLOCK;
    }    

    pxTemp = (uint8_t *)pxInsert_BLOCK;

    if (pxTemp + pxInsert_BLOCK->SizeOfBLOCK == (uint8_t *)pxStart_BLOCK->pNext_FreeBLOCK) {
        if (pxStart_BLOCK->pNext_FreeBLOCK != pEndBLOCK) {
            pxInsert_BLOCK->SizeOfBLOCK += pxStart_BLOCK->pNext_FreeBLOCK->SizeOfBLOCK;
            pxInsert_BLOCK->pNext_FreeBLOCK = pxStart_BLOCK->pNext_FreeBLOCK->pNext_FreeBLOCK;
        }
        else {
            pxInsert_BLOCK->pNext_FreeBLOCK = pEndBLOCK;
        }
    }
    else {
        pxInsert_BLOCK->pNext_FreeBLOCK = pxStart_BLOCK->pNext_FreeBLOCK;
    }

    if (pxStart_BLOCK != pxInsert_BLOCK) {
        pxStart_BLOCK->pNext_FreeBLOCK = pxInsert_BLOCK;
    }
}


/**
 * ==================================
 * @function: prvLink_FromInsertBLOCK
 */
void prvLink_FromInsertBLOCK(BlockLink_t *pxInsert_BLOCK, BlockLink_t *pxStart_BLOCK) {
    uint8_t *pxTemp = (uint8_t *)pxInsert_BLOCK;
    BlockLink_t *pxNewStart_BLOCK = &StartBLOCK;

    if (pxTemp + pxInsert_BLOCK->SizeOfBLOCK == (uint8_t *)pxStart_BLOCK) {
        pxInsert_BLOCK->SizeOfBLOCK += pxStart_BLOCK->SizeOfBLOCK;
        pxInsert_BLOCK->pNext_FreeBLOCK = pxStart_BLOCK->pNext_FreeBLOCK;
    }
    else {
        pxInsert_BLOCK->pNext_FreeBLOCK = pxStart_BLOCK;
    }

    // Setup new start BLOCK
    pxNewStart_BLOCK->pNext_FreeBLOCK = pxInsert_BLOCK;
}


/**
 * ======================================
 * @function: DEBUG_ListFreeBLOCKTracking
 */
void DEBUG_ListFreeBLOCKTracking() {
    ENTRY_CRITICAL();

    BlockLink_t *pxRun_BLOCK = StartBLOCK.pNext_FreeBLOCK;

    SYS_PRINT("\t\t======= List Free BLOCK Tracking =======\n");
    SYS_PRINT("[START][%d][%d][%d] -> ", (uint32_t)pxRun_BLOCK, (uint32_t)pxRun_BLOCK + BLOCK_LINK_STRUCT_SIZE, pxRun_BLOCK->SizeOfBLOCK);
    while (1) {
        if (pxRun_BLOCK->pNext_FreeBLOCK == pEndBLOCK) {
            SYS_PRINT("[END]\n");
            break;
        }
        else {
            pxRun_BLOCK = pxRun_BLOCK->pNext_FreeBLOCK;
            SYS_PRINT("[%d][%d][%d] -> ", (uint32_t)pxRun_BLOCK, (uint32_t)pxRun_BLOCK + BLOCK_LINK_STRUCT_SIZE, pxRun_BLOCK->SizeOfBLOCK);
        }
    }

    EXIT_CRITICAL();
}


/**
 * =======================================
 * @function: DEBUG_AllocatedBLOCKTracking
 */
void DEBUG_AllocatedBLOCKTracking() {
    ENTRY_CRITICAL();

    // Increasement sorting by address BLOCK
    qsort(DbgBLOCK.BLOCKS, sizeof(DbgBLOCK.BLOCKS) / sizeof(DbgBLOCK.BLOCKS[0]), sizeof(DbgBLOCK.BLOCKS[0]), (int (*) (const void *, const void *)) &prvBLOCKCompare);

    SYS_PRINT("\t\t======= Allocated BLOCK Tracking =======\n");
    uint8_t idx = 0;
    for (; idx < DEBUG_BLOCK_TOTAL; ++idx) {
        if (ALLOCATED == DbgBLOCK.BLOCKS[idx].Status_BLOCK) {
            SYS_PRINT("[%d][%d][%d]\t", DbgBLOCK.BLOCKS[idx].Address_BLOCK, DbgBLOCK.BLOCKS[idx].DataAddress_BLOCK, DbgBLOCK.BLOCKS[idx].SizeOfBLOCK);
        }
    }
    SYS_PRINT("\n\n");

    EXIT_CRITICAL();
}


/**
 * =============================
 * @function: DEBUG_HeapTracking
 */
void DEBUG_HeapTracking() {
    DEBUG_ListFreeBLOCKTracking();
    DEBUG_AllocatedBLOCKTracking();
}

/**
 * ==========================
 * @function: prvBLOCKCompare
 */
int prvBLOCKCompare (const DebugBLOCK_t *BLOCK_A, const DebugBLOCK_t *BLOCK_B) {
	return (BLOCK_A->Address_BLOCK < BLOCK_B->Address_BLOCK) ? ( -1 ) : ( 1 );
}
