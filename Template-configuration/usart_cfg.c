/**
 * =======================
 * @function: UART_Config
 */
void UART_Config() {
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	GPIO_PinAFConfig(GPIOD, GPIO_PinSource5, GPIO_AF_USART2);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource6, GPIO_AF_USART2);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART2, &USART_InitStructure);

	USART_ITConfig (USART2, USART_IT_RXNE, ENABLE);
	USART_ITConfig (USART2, USART_IT_TXE, DISABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_Cmd(USART2, ENABLE);
}


/**
 * ========================
 * @function: UART_SendChar
 */
void UART_SendChar(char c) {
	while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
	USART_SendData(USART2, c);
	while (USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
}


/**
 * ==========================
 * @function: UART_SendString
 */
void UART_SendString(const char * str) {
	uint8_t idx = 0;

	while (str[idx] != '\0') {
		USARTx_SendChar(USART2, str[idx]);
		++idx; 
	}
}

/**
 * ===========================
 * @function: UART_ReceiveChar
 */
char UART_ReceiveChar() {
	char ret;
	while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE) != SET);
	ret = (char)USART_ReceiveData(USART2);

	return ret;
}

/**
 * ============================
 * @function: USART2_IRQHandler
 */
void USART2_IRQHandler() {
	// RX
	if (USART_GetITStatus(USART2, USART_IT_RXNE) == SET) {
		char c = USART_ReceiveData(USART2);
		/* TO DO */
	}

	// TX
	if (USART_GetITStatus(USART2, USART_IT_TXE) == SET) {
		USART_SendData(USART2, c);
		USART_ClearITPendingBit(USART2, USART_IT_TXE);
		// If needed
		USART_ITConfig (USART2, USART_IT_TXE, DISABLE);
	}
}
