 /**
 * =========================
 * @function: IO_OuputConfig
 */
void IO_OuputConfig(void) {
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
}

/**
 * =====================
 * @function: IO_SetHigh
 */
void IO_SetHigh() {
    GPIO_SetBits(USER_LED_PORT, GPIO_Pin_12);
}

/**
 * =====================
 * @function: IO_SetLow
 */
void IO_SetLow() {
    GPIO_ResetBits(GPIOD, GPIO_Pin_12);
}

/**
 * ========================
 * @function: IO_SetToggle
 */
void IO_SetToggle() {
    GPIO_ToggleBits(GPIOD, GPIO_Pin_12);
}


/**
 * =========================
 * @function: IO_InputConfig
 */
void IO_InputConfig() {
    GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
  	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	/* Configure GPIOx */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);

	EXTI_InitStructure.EXTI_Line = EXTI_Line0;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}


/**
 * =========================
 * @function: IO_InputConfig
 */
void IO_ReadInput() {
	return GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0);
}

void EXT0_IRQHanler() {
	if (EXTI_GetFlagStatus(EXTI_Line0) == SET) {
		while (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0));
		EXTI_ClearFlag(EXTI_Line0);
		EXTI_ClearITPendingBit(EXTI_Line0);
		/* TO DO */
	}
}