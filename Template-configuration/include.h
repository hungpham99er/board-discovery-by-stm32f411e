/*========================================================================
 * #### IO function prototypes ####
 =========================================================================*/
extern void IO_OutputConfig(void);
extern void IO_SetLow(void);
extern void IO_SetHigh(void);
extern void IO_SetToggle(void);

extern void IO_InputConfig(void);
extern void IO_ReadInput(void);

extern void EXT0_IRQHanler();

 /*========================================================================
 * #### UART function prototypes ####
 =========================================================================*/
void UART_Config(void);
void UART_SendChar(char);
void UART_SendChar(const char *);
char UART_ReceiveChar(void);

void USART2_IRQHandler(void);

 /*========================================================================
 * #### SYSTICK TIMER function prototypes ####
 =========================================================================*/
void SYSTICK_Config(void);

void TIM3_PeriodConfig(void);
void TIM3_IRQHandler(void);

void TIM4_PWMConfig(void);
void TIM4_SetPWM(uint8_t percent);

void TIM9_OCConfig(void);
void TIM1_BRK_TIM9_IRQHandler(void);

void TIM10_ICConfig(void);
void TIM1_UPT_TIM10_IRQHandler(void);

 /*========================================================================
 * #### WATCHDOG function prototypes ####
 =========================================================================*/
void IWDG_Config(void);
void IWDG_Refresh(void);

void WWDG_Config(void);
void WWDG_Refresh(uint8_t val);

 /*========================================================================
 * #### DMA function prototypes ####
 =========================================================================*/
extern void DMA_MemToMem_Config(void);
extern void DMA_MemToPer_Config(void);
extern void DMA_PerToMem_Config(void);


/*========================================================================
 * #### RTC function prototypes ####
 =========================================================================*/
void RTC_BackUp_Config(void);
void RTC_Write_BackUp_Data(void);
void RTC_Read_BackUp_Data(void);

void RTC_WakeUpConfig(void);
void RTC_WKUP_IRQHandler(void);

void RTC_AlarmConfig(void);
void RTC_Alarm_IRQHandler(void);



/*========================================================================
* #### ADC function prototypes ####
=========================================================================*/
extern void ADC_SingleChannelConfig(void);
extern void ADC_MultiChannelConfig(void);
extern uint16_t ADC_ReadSingleChannel(void);

extern void ADC_IRQHandler(void);