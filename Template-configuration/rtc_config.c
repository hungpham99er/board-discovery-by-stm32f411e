/*========================================================================
 * #### RTC Backup ####
 =========================================================================*/
const uint32_t BK_Register[20] = {
	RTC_BKP_DR0,  RTC_BKP_DR1,  RTC_BKP_DR2,
	RTC_BKP_DR3,  RTC_BKP_DR4,  RTC_BKP_DR5,
	RTC_BKP_DR6,  RTC_BKP_DR7,  RTC_BKP_DR8,
	RTC_BKP_DR9,  RTC_BKP_DR10, RTC_BKP_DR11,
	RTC_BKP_DR12, RTC_BKP_DR13, RTC_BKP_DR14,
	RTC_BKP_DR15, RTC_BKP_DR16, RTC_BKP_DR17,
	RTC_BKP_DR18, RTC_BKP_DR19,
};

/**
 * =============================
 * @function: RTC_BackUp_Config
 */
void RTC_BackUp_Config() {
	// Reset RTC Configuration back to default
	RTC_DeInit();

	// Enable the PWR clock
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

	// Allow access to the backup domain of RTC
	PWR_BackupAccessCmd(ENABLE);

	// Select the RTC Clock Source
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);

	// Enable the RTC Clock
	RCC_RTCCLKCmd(ENABLE);

	// Wait for RTC APB registers synchronisation
	RTC_WaitForSynchro();
}

/**
 * ================================
 * @function: RTC_Write_BackUp_Data
 */
void RTC_Write_BackUp_Data(uint8_t rg_idx, uint32_t _data) {
	RTC_WriteBackupRegister(BK_Register[rg_idx], _data);
}

/**
 * ===============================
 * @function: RTC_Read_BackUp_Data
 */
uint32_t RTC_Read_BackUp_Data(uint8_t rg_idx) {
	return RTC_ReadBackupRegister(BK_Register[rg_idx]);
}

/*========================================================================
 * #### RTC WAKEUP ####
 =========================================================================*/
#define RTC_WKP_INTERRUPT_INTERVAL		1 /* 1 -> 65536 */
#define RTC_WKP_COUNTER_VAL				(RTC_WKP_INTERRUPT_INTERVAL - 1)
#define RTC_WKP_ASYNC_PRES_DIV			0x7F // 127
#define RTC_WKP_SYNC_PRES_DIV			0xFF // 255
#define RTC_WKP_HOUR_FMT				RTC_HourFormat_24

/**
 * ===============================
 * @function: RTC_WakeUpConfig
 */
void RTC_WakeUpConfig() {
	NVIC_InitTypeDef NVIC_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	RTC_InitTypeDef RTC_InitStructure;


	RTC_DeInit();
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
	PWR_BackupAccessCmd(ENABLE);
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
	RCC_RTCCLKCmd(ENABLE);
	RTC_WaitForSynchro();


	RTC_InitStructure.RTC_AsynchPrediv = RTC_WKP_ASYNC_PRES_DIV;
	RTC_InitStructure.RTC_SynchPrediv  = RTC_WKP_SYNC_PRES_DIV;
	RTC_InitStructure.RTC_HourFormat = RTC_WKP_HOUR_FMT;
	RTC_Init(&RTC_InitStructure);

	EXTI_ClearITPendingBit(EXTI_Line22);
	EXTI_InitStructure.EXTI_Line = EXTI_Line22;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = RTC_WKUP_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	RTC_WakeUpCmd(DISABLE);

	/* Configure the RTC WakeUp Clock source: CK_SPRE (1Hz) *****/
	RTC_WakeUpClockConfig(RTC_WakeUpClock_CK_SPRE_16bits);
	RTC_SetWakeUpCounter(RTC_WKP_COUNTER_VAL); /* @note: Count down value */

	RTC_ITConfig(RTC_IT_WUT, ENABLE);

	RTC_WakeUpCmd(ENABLE);
}

/**
 * ============================
 * @function: RTC_WakeUpConfig
 */
void RTC_WKUP_IRQHandler() {
	if (RTC_GetITStatus(RTC_IT_WUT) == SET) {
		RTC_ClearITPendingBit(RTC_IT_WUT);
		EXTI_ClearITPendingBit(EXTI_Line22);
		/* TO DO */
	}
}

/*========================================================================
 * #### RTC ALARM ####
 =========================================================================*/
#define RTC_ALARM_ASYNC_PRES_DIV		0x7F // 127
#define RTC_ALARM_SYNC_PRES_DIV			0xFF // 255
#define RTC_ALARM_HOUR_FMT				RTC_HourFormat_24

/**
 * ===========================
 * @function: RTC_AlarmConfig
 */
void RTC_AlarmConfig() {
	NVIC_InitTypeDef NVIC_InitStructure;
	RTC_InitTypeDef RTC_InitStructure;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	RTC_AlarmTypeDef RTC_AlarmStructure;


	RTC_DeInit();
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
	PWR_BackupAccessCmd(ENABLE); /* Allow access to RTC */
#if 0
	/* Reset BKP Domain */
	RCC_BackupResetCmd(ENABLE);
	RCC_BackupResetCmd(DISABLE);
#endif

	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
	RCC_RTCCLKCmd(ENABLE);
	RTC_WaitForSynchro();

	RTC_InitStructure.RTC_AsynchPrediv = RTC_ALARM_ASYNC_PRES_DIV;
	RTC_InitStructure.RTC_SynchPrediv  = RTC_ALARM_SYNC_PRES_DIV;
	RTC_InitStructure.RTC_HourFormat = RTC_ALARM_HOUR_FMT;
	RTC_Init(&RTC_InitStructure);

	// Set the date: Sunday February 6th 2022
	RTC_DateStructure.RTC_Year = 22;
	RTC_DateStructure.RTC_Month = RTC_Month_February;
	RTC_DateStructure.RTC_Date = 6;
	RTC_DateStructure.RTC_WeekDay = RTC_Weekday_Sunday;
	RTC_SetDate(RTC_Format_BIN, &RTC_DateStructure);

	// Set the time to 23h 59mn 45s PM 
	RTC_TimeStructure.RTC_H12 = RTC_H12_PM;
	RTC_TimeStructure.RTC_Hours = 23;
	RTC_TimeStructure.RTC_Minutes = 59;
	RTC_TimeStructure.RTC_Seconds = 45;
	RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure);

	EXTI_ClearITPendingBit(EXTI_Line17);
	EXTI_InitStructure.EXTI_Line = EXTI_Line17;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = RTC_Alarm_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	RTC_AlarmCmd(RTC_Alarm_A, DISABLE);

	// Set the alarm 00h:01min:15s
	RTC_AlarmStructure.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay; /* Do not care in compare */
	RTC_AlarmStructure.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
	RTC_AlarmStructure.RTC_AlarmDateWeekDay = 7;
	RTC_AlarmStructure.RTC_AlarmTime.RTC_H12 = RTC_H12_PM;
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours = 0;
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = 0;
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = 10;
	RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_AlarmStructure);

	RTC_ClearITPendingBit(RTC_IT_ALRA);
	EXTI_ClearITPendingBit(EXTI_Line17);

	RTC_ITConfig(RTC_IT_ALRA, ENABLE);

	if (RTC_AlarmCmd(RTC_Alarm_A, ENABLE) == ERROR) {
		// FATAL
	}
}

/**
 * ===============================
 * @function: RTC_Alarm_IRQHandler
 */
void RTC_Alarm_IRQHandler() {
	static uint8_t alarm_minute_update = 0;

	if(RTC_GetITStatus(RTC_IT_ALRA) == SET) {
		RTC_ClearITPendingBit(RTC_IT_ALRA);
		RTC_ClearFlag (RTC_IT_ALRA);
		EXTI_ClearITPendingBit(EXTI_Line17);
		/* TO DO */
		alarm_minute_update = (alarm_minute_update == 60) ? 0 : alarm_minute_update + 1;

		// Update AlarmA
		RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
		RTC_ITConfig(RTC_IT_ALRA, DISABLE);
		RTC_AlarmTypeDef RTC_UpdateAlarmStructure;
		RTC_UpdateAlarmStructure.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay; /* Do not care in compare */
		RTC_UpdateAlarmStructure.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
		RTC_UpdateAlarmStructure.RTC_AlarmDateWeekDay = 7;
		RTC_UpdateAlarmStructure.RTC_AlarmTime.RTC_H12 = RTC_H12_PM;
		RTC_UpdateAlarmStructure.RTC_AlarmTime.RTC_Hours = 0;
		RTC_UpdateAlarmStructure.RTC_AlarmTime.RTC_Minutes = alarm_minute_update;
		RTC_UpdateAlarmStructure.RTC_AlarmTime.RTC_Seconds = 10;
		RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_UpdateAlarmStructure);

		RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
		RTC_ITConfig(RTC_IT_ALRA, ENABLE);
	}
}
