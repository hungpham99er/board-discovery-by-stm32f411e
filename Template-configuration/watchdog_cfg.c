/**
 * ===========================
 * @function: IWDG_Config
 */
void IWDG_Config() {
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	IWDG_SetPrescaler(IWDG_PRESCALER_FREQ); // MUST-BE Modify

	IWDG_SetReload(IWDG_RELOAD_VALUE);  // MUST-BE Modify

	IWDG_ReloadCounter();
	IWDG_Enable();
}

/**
 * =========================
 * @function: IWDG_Refresh
 */
void IWDG_Refresh() {
	IWDG_ReloadCounter();
}

/**
 * =======================
 * @function: WWDG_Config
 */
void WWDG_Config() {
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);

	/* WWDG clock counter = (PCLK1 (50MHz)/4096)/8 = 1525 Hz (~655 us)  */
	WWDG_SetPrescaler(WWDG_PRESCALER_FREQ); //  MUST-BE Modify

	/* Set Window value to WWDG_SET_VALUE; WWDG counter should be refreshed only when the counter
	is below 80 (and greater than 64(0x3F)) otherwise a reset will be generated */
	WWDG_SetWindowValue(WWDG_VALUE_SET); //  MUST-BE Modify

	/* Enable WWDG and set counter value to 127, WWDG timeout = ~655 us * 64(0x3F) = 41.92 ms
	 In this case the refresh window is:
		   ~655 * (127-80) = 30.78ms < refresh window < ~655 * 64 = 41.92ms
	*/
	WWDG_Enable(WWDG_VALUE_LOAD); //  MUST-BE Modify
}

/**
 * ========================
 * @function: WWDG_Refresh
 */
void WWDG_Refresh(uint8_t val) {
	WWDG_SetCounter(val);
}










