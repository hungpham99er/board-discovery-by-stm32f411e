#define BUFFER_SIZE_INTEGER				32U
#define BUFFER_SIZE_CHAR				8U

/*========================================================================
 * #### MEM TO MEM ####
 =========================================================================*/
#define DMA_MemToMem_STREAM_CLOCK_GATE		RCC_AHB1Periph_DMA2
#define DMA_MemToMem_STREAM					DMA2_Stream0
#define DMA_MemToMem_CHANNEL				DMA_Channel_0
#define DMA_MemToMem_SOURCE_ADDR			(uint32_t)Flash_RegionData_UI32
#define DMA_MemToMem_DESTINATION_ADDR		(uint32_t)SRAM_RegionData_UI32

uint32_t SRAM_RegionData_UI32[BUFFER_SIZE_INTEGER];
const uint32_t Flash_RegionData_UI32[BUFFER_SIZE_INTEGER]= {
									1,  9, 17, 25,
									2, 10, 18, 26,
									3, 11, 19, 27,
									4, 12, 20, 28,
									5, 13, 21, 29,
									6, 14, 22, 30,
									7, 15, 23, 31,
									8, 16, 24, 32
									};
char SRAM_RegionData_Char[BUFFER_SIZE_CHAR];
const char Flash_RegionData_Char[BUFFER_SIZE_CHAR] = {
									'a', 'b', 'c', 'd',
									'e', 'f', 'g', 'h'
									};

/**
 * ==============================
 * @function: DMA_MemToMem_Config
 */
void DMA_MemToMem_Config() {
	NVIC_InitTypeDef NVIC_InitStructure;
	DMA_InitTypeDef  DMA_InitStructure;

	/* Reset DMA Stream registers return Init status */
	DMA_DeInit(DMA_MemToMem_STREAM);

	/* Enable DMA clock */
	RCC_AHB1PeriphClockCmd(DMA_MemToMem_STREAM_CLOCK_GATE, ENABLE);

	/* Check if the DMA Stream is disabled before enabling it.
	 Note that this step is useful when the same Stream is used multiple times:
	 enabled, then disabled then re-enabled... In this case, the DMA Stream disable
	 will be effective only at the end of the ongoing data transfer and it will
	 not be possible to re-configure it before making sure that the Enable bit
	 has been cleared by hardware. If the Stream is used only once, this step might
	 be bypassed. */
	while (DMA_GetCmdStatus(DMA_MemToMem_STREAM) != DISABLE)
	{
	}

	/* Configure DMA Stream */
	DMA_InitStructure.DMA_Channel = DMA_MemToMem_CHANNEL;
	DMA_InitStructure.DMA_PeripheralBaseAddr = DMA_MemToMem_SOURCE_ADDR;		/* SOURCE		*/
	DMA_InitStructure.DMA_Memory0BaseAddr = DMA_MemToMem_DESTINATION_ADDR;		/* DESTINATION */
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToMemory;
	DMA_InitStructure.DMA_BufferSize = (uint32_t)BUFFER_SIZE_INTEGER;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Enable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal; /* Non Circular */
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable; /* The Direct mode */
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA_MemToMem_STREAM, &DMA_InitStructure);

	/* Enable DMA Stream Transfer Complete interrupt */
	DMA_ITConfig(DMA_MemToMem_STREAM, DMA_IT_TC, ENABLE);

	/* Enable the DMA Stream IRQ Channel */
	NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 7;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* DMA Stream enable */
	DMA_Cmd(DMA_MemToMem_STREAM, ENABLE);

	/* Check if the DMA Stream has been effectively enabled.
	 The DMA Stream Enable bit is cleared immediately by hardware if there is an
	 error in the configuration parameters and the transfer is no started (ie. when
	 wrong FIFO threshold is configured ...) */
	while ((DMA_GetCmdStatus(DMA_MemToMem_STREAM) != ENABLE)) {
		ERROR("DMA_GetCmdStatus(DMA_STREAM) != ENABLE");
	}
}

 /*========================================================================
 * #### MEM TO PER ####
 =========================================================================*/
#define DMA_MemToPer_PWM_VALUE_LOAD_TOTAL	5U

#define DMA_MemToPer_STREAM_CLOCK_GATE		RCC_AHB1Periph_DMA2
#define DMA_MemToPer_STREAM					DMA2_Stream6
#define DMA_MemToPer_CHANNEL				DMA_Channel_6
#define DMA_MemToPer_SOURCE_ADDR			(uint32_t)DMA_MemToPer_Container_PWMValueLoad
#define DMA_MemToPer_DESTINATION_ADDR		(uint32_t)(&TIM1->CCR3)
#define DMA_MemToPer_BUFFER_SIZE			5U

#define DMA_MemToPer_TIM1_CLOCK_GATE		RCC_APB2Periph_TIM1
#define DMA_MemToPer_TIM1_PWM_PERIOD		999
#define DMA_MemToPer_TIM1_PWM_PRESCALER		99
#define DMA_MemToPer_TIM1_PWM_REPETITION	3

#define DMA_MemToPer_IO_CLOCK_GATE			RCC_AHB1Periph_GPIOA
#define DMA_MemToPer_IO_PIN					GPIO_Pin_10
#define DMA_MemToPer_IO_PORT				GPIOA
#define DMA_MemToPer_IO_PIN_SOURCE			GPIO_PinSource10

/**
 * ==============================
 * @function: DMA_MemToPer_Config
 */
void DMA_MemToPer_Config() {
	GPIO_InitTypeDef GPIO_InitStructure;
	DMA_InitTypeDef DMA_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;

	RCC_AHB1PeriphClockCmd(DMA_MemToPer_IO_CLOCK_GATE, ENABLE);
	RCC_AHB1PeriphClockCmd(DMA_PerToMem_STREAM_CLOCK_GATE , ENABLE);
	RCC_APB2PeriphClockCmd(DMA_MemToPer_TIM1_CLOCK_GATE, ENABLE);

	GPIO_InitStructure.GPIO_Pin = DMA_MemToPer_IO_PIN ;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(DMA_MemToPer_IO_PORT, &GPIO_InitStructure);

	GPIO_PinAFConfig(DMA_MemToPer_IO_PORT, DMA_MemToPer_IO_PIN_SOURCE, GPIO_AF_TIM1);

	DMA_DeInit(DMA_PerToMem_STREAM);

	DMA_InitStructure.DMA_Channel = DMA_MemToPer_CHANNEL;
	DMA_InitStructure.DMA_PeripheralBaseAddr = DMA_MemToPer_DESTINATION_ADDR;
	DMA_InitStructure.DMA_Memory0BaseAddr = DMA_MemToPer_SOURCE_ADDR;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_BufferSize = DMA_MemToPer_BUFFER_SIZE;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA_MemToPer_STREAM, &DMA_InitStructure);

	// Set up value PWM load
	DMA_MemToPer_Container_PWMValueLoad[0] = ((0 * (DMA_MemToPer_TIM1_PWM_PERIOD + 1)) / 100 );
	DMA_MemToPer_Container_PWMValueLoad[1] = ((25 * (DMA_MemToPer_TIM1_PWM_PERIOD + 1)) / 100 ) - 1;
	DMA_MemToPer_Container_PWMValueLoad[2] = ((50 * (DMA_MemToPer_TIM1_PWM_PERIOD + 1)) / 100 ) - 1;
	DMA_MemToPer_Container_PWMValueLoad[3] = ((75 * (DMA_MemToPer_TIM1_PWM_PERIOD + 1)) / 100 ) - 1;
	DMA_MemToPer_Container_PWMValueLoad[4] = ((100 * (DMA_MemToPer_TIM1_PWM_PERIOD + 1)) / 100 ) - 1;

	// TIM1 Configuration 1KHz 
	TIM_TimeBaseStructure.TIM_Prescaler = DMA_MemToPer_TIM1_PWM_PRESCALER;
	TIM_TimeBaseStructure.TIM_Period = DMA_MemToPer_TIM1_PWM_PERIOD;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 3;
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);

	// Channel 3 Configuration in PWM mode
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = DMA_MemToPer_Container_PWMValueLoad[0];
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
	TIM_OC3Init(TIM1, &TIM_OCInitStructure);

	TIM_OC3PreloadConfig(TIM1, TIM_OCPreload_Enable);

	TIM_Cmd(TIM1, ENABLE);

	/* DMA enable*/
	DMA_Cmd(DMA_MemToPer_STREAM, ENABLE);

	/* TIM1 Update DMA Request enable */
	TIM_DMACmd(TIM1, TIM_DMA_CC3, ENABLE);

	/* Main Output Enable */
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
}

 /*========================================================================
 * #### PER TO MEM ####
 =========================================================================*/
#define DMA_PerToMem_ADC_TOTAL				2

#define DMA_PerToMem_STREAM_CLOCK_GATE		RCC_AHB1Periph_DMA2
#define DMA_PerToMem_STREAM					DMA2_Stream4
#define DMA_PerToMem_CHANNEL				DMA_Channel_0
#define DMA_PerToMem_PERIPHERAL_ADDR		(uint32_t)(&ADC1->DR)
#define DMA_PerToMem_MEMORY_ADDR			(uint32_t)&DMA_PerToMem_Container_ADCValueReturn;
#define DMA_PerToMem_BUFFER_SIZE			2U

#define DMA_PerToMem_ADC_CLOCK_GATE			RCC_APB2Periph_ADC1
#define DMA_PerToMem_ADC_CHANNEL_1			ADC_Channel_14
#define DMA_PerToMem_ADC_CHANNEL_2			ADC_Channel_15
#define DMA_PerToMem_ADC_RANK_CHNL_1		1
#define DMA_PerToMem_ADC_RANK_CHNL_2		2
#define DMA_PerToMem_ADC					ADC1

#define DMA_PerToMem_ADC_IO_CLOCK_GATE		RCC_AHB1Periph_GPIOC
#define DMA_PerToMem_ADC_PIN_1				GPIO_Pin_4
#define DMA_PerToMem_ADC_PIN_2				GPIO_Pin_5
#define DMA_PerToMem_ADC_PORT				GPIOC

uint16_t DMA_PerToMem_Container_ADCValueReturn[DMA_PerToMem_ADC_TOTAL];
uint16_t DMA_MemToPer_Container_PWMValueLoad[DMA_MemToPer_PWM_VALUE_LOAD_TOTAL];

/**
 * =============================
 * @function: DMA_PerToMem_Config
 */
void DMA_PerToMem_Config() {
	ADC_InitTypeDef ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;
	DMA_InitTypeDef DMA_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Reset DMA Stream registers return Init status */
	DMA_DeInit(DMA_PerToMem_STREAM);

	RCC_AHB1PeriphClockCmd(DMA_PerToMem_STREAM_CLOCK_GATE, ENABLE);
	RCC_AHB1PeriphClockCmd(DMA_PerToMem_ADC_IO_CLOCK_GATE, ENABLE);
	RCC_APB2PeriphClockCmd(DMA_PerToMem_ADC_CLOCK_GATE, ENABLE);

	/* Wait for last transfer complete */
	while (DMA_GetCmdStatus(DMA_PerToMem_STREAM) != DISABLE)
	{
	}

	DMA_InitStructure.DMA_Channel = DMA_PerToMem_CHANNEL;
	DMA_InitStructure.DMA_PeripheralBaseAddr = DMA_PerToMem_PERIPHERAL_ADDR;
	DMA_InitStructure.DMA_Memory0BaseAddr = DMA_PerToMem_MEMORY_ADDR;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_BufferSize = DMA_PerToMem_BUFFER_SIZE;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord; /* uint16_t */
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord; /* uint16_t */
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA_PerToMem_STREAM, &DMA_InitStructure);

	/* DMA Enable */
	DMA_Cmd(DMA_PerToMem_STREAM, ENABLE);

	GPIO_InitStructure.GPIO_Pin = DMA_PerToMem_ADC_PIN_1 | DMA_PerToMem_ADC_PIN_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(DMA_PerToMem_ADC_PORT, &GPIO_InitStructure);

	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = ENABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = 2;
	ADC_Init(DMA_PerToMem_ADC, &ADC_InitStructure);

	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_CommonInitStructure);

	ADC_RegularChannelConfig(DMA_PerToMem_ADC, DMA_PerToMem_ADC_CHANNEL_1,
							DMA_PerToMem_ADC_RANK_CHNL_1, ADC_SampleTime_15Cycles);
	ADC_RegularChannelConfig(DMA_PerToMem_ADC, DMA_PerToMem_ADC_CHANNEL_2,
							DMA_PerToMem_ADC_RANK_CHNL_2, ADC_SampleTime_15Cycles);

	/* Enable DMA request after last transfer (Single-ADC mode) */
	ADC_DMARequestAfterLastTransferCmd(DMA_PerToMem_ADC, ENABLE);

	/* Enable ADC DMA */
	ADC_DMACmd(DMA_PerToMem_ADC, ENABLE);

	/* Enable ADC */
	ADC_Cmd(DMA_PerToMem_ADC, ENABLE);

	/* Start ADC Software Conversion */
	ADC_SoftwareStartConv(DMA_PerToMem_ADC);
}
