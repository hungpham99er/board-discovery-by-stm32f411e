/**
 * ========================= [___FORMULAR__] =========================
 *								SystemCoreClock
 * F_timer_counter =	____________________________________
 *						(TIM_Prescaler + 1)x(TIM_Period + 1)
 *
 *
 *				(TIM_Period + 1) * Duty_Cycle_PWM
 * TIM_Pulse =	___________________________________ - 1
 *								100
 *
 * =====================================================================
 **/

/*========================================================================
 * #### SYSTICK ####
 =========================================================================*/
/**
 * ==========================
 * @function: SYSTICK_Config
 */
void SYSTICK_Config() {
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / TICK_1_MILISECOND); // TICK_1_MILISECOND = 1000
	// Set Priority for Systick Interrupt
	NVIC_SetPriority (SysTick_IRQn, 1); 
}

/*========================================================================
 * #### TIMER PERIOD ####
 =========================================================================*/
/**
 * ===========================
 * @function: TIMER_PeriodConfig
 */
void TIM3_PeriodConfig() {
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

	TIM_TimeBaseStructure.TIM_Period 	= ?????????????;
	TIM_TimeBaseStructure.TIM_Prescaler = ?????????????;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_Cmd(TIM3, ENABLE);
}

/**
 * ===========================
 * @function: TIM3_IRQHandler
 */
void TIM3_IRQHandler() {
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) == SET) {
		TIM_ClearFlag(TIM3, TIM_IT_Update);
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
		/* TO DO */

	}
}

/*========================================================================
 * #### TIMER PWM ####
 =========================================================================*/
/**
 * ==========================
 * @function: TIM4_PWMConfig
 */
void TIM4_PWMConfig() {
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	GPIO_PinAFConfig(GPIOD, GPIO_PinSource12, GPIO_AF_TIM4);

	TIM_TimeBaseStructure.TIM_Period 	= ????????????; // MUST-BE Modify
	TIM_TimeBaseStructure.TIM_Prescaler = ????????????; // MUST-BE Modify
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);

	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = ????????????; // MUST-BE Modify
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC1Init(TIM4, &TIM_OCInitStructure);

	TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);
	TIM_Cmd(TIM4, ENABLE);
}

/**
 * =======================
 * @function: TIM4_SetPWM
 */
void TIM4_SetPWM(uint8_t percent) {
	if (percent < 0 || percent > 100) {
		// FATAL
	}

	uint32_t val_load = (((TIM4->ARR) * percent) / 100);

	// Set PWM
	TIM_SetCompare1(TIM4, val_load);
}

/*========================================================================
 * #### TIMER OUTPUT COMPARE ####
 =========================================================================*/
/**
 * =========================
 * @function: TIM9_OCConfig
 */
void TIM9_OCConfig() {
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM9, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA , ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_TIM9);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_TIM9);

	TIM_TimeBaseStructure.TIM_Period 	= TIM9_PERIOD  	 // MUST-BE Modify
	TIM_TimeBaseStructure.TIM_Prescaler = TIM9_PRESCALER // MUST-BE Modify
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM9, &TIM_TimeBaseStructure);

	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Toggle;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = TIM9_OC_PULSE_CHANNEL_1; // MUST-BE Modify
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC1Init(TIM9, &TIM_OCInitStructure);

	TIM_OC1PreloadConfig(TIM9, TIM_OCPreload_Disable);

	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = TIM9_OC_PULSE_CHANNEL_2; // MUST-BE Modify
	TIM_OC2Init(TIM9, &TIM_OCInitStructure);

	TIM_OC2PreloadConfig(TIM9, TIM_OCPreload_Disable);

	TIM_ClearITPendingBit (TIM9, TIM_IT_CC1 | TIM_IT_CC2);
	TIM_ITConfig(TIM9, TIM_IT_CC1 | TIM_IT_CC2, ENABLE);

	NVIC_InitStructure.NVIC_IRQChannel = TIM1_BRK_TIM9_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_SetCounter(TIM9, 0);
	TIM_Cmd(TIM9, ENABLE);
}

/**
 * ===================================
 * @function: TIM1_BRK_TIM9_IRQHandler
 */
void TIM1_BRK_TIM9_IRQHandler() {
	uint32_t oc_tim9_ccrx;  // TIM9->CCRx value
	uint32_t oc_reload; 	// Output compare reload value

	// OC TIM9_CH1
	if (TIM_GetITStatus(TIM9, TIM_IT_CC1) == SET) {
		TIM_ClearFlag(TIM9, TIM_IT_CC1);
		TIM_ClearITPendingBit(TIM9, TIM_IT_CC1);
		/* TO DO */
		oc_tim9_ccrx = TIM_GetCapture1(TIM9);
		oc_reload = oc_tim9_ccrx + TIM9_OC_PULSE_CHANNEL_1;

		if (oc_reload < TIM9_OC_PERIOD) {
			TIM_SetCompare1(TIM9, oc_reload);
		}
		else {
			TIM_SetCompare1(TIM9, TIM9_OC_PULSE_CHANNEL_1);
		}
	}

	// OC TIM9_CH2 
	if (TIM_GetITStatus(TIM9, TIM_IT_CC2) == SET) {
		TIM_ClearFlag(TIM9, TIM_IT_CC2);
		TIM_ClearITPendingBit(TIM9, TIM_IT_CC2);
		/* TO DO */
		oc_tim9_ccrx = TIM_GetCapture2(TIM9);
		oc_reload = oc_tim9_ccrx + TIM9_OC_PULSE_CHANNEL_2;

		/* Check invalid value ******/
		if (oc_reload < TIM9_OC_PERIOD) {
			TIM_SetCompare2(TIM9, oc_reload);
		}
		else {
			TIM_SetCompare2(TIM9, TIM9_OC_PULSE_CHANNEL_2);
		}

	}
}

/*========================================================================
 * #### TIMER INPUT CAPTURE ####
 =========================================================================*/
/**
 * =========================
 * @function: TIM10_ICConfig
 */
void TIM10_ICConfig() {
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_ICInitTypeDef TIM_ICInitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM10, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_TIM10);

	TIM_TimeBaseStructure.TIM_Period = TIM10_IC_PERIOD; 	  // MUST-BE Modify
	TIM_TimeBaseStructure.TIM_Prescaler = TIM10_IC_PRESCALER; // MUST-BE Modify
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM10, &TIM_TimeBaseStructure);

	TIM_ICInitStructure.TIM_Channel = TIM10_IC_CHANNEL_1;
	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	TIM_ICInitStructure.TIM_ICFilter = 0x0;
	TIM_ICInit(TIM10, &TIM_ICInitStructure);

	TIM_ClearITPendingBit (TIM10, TIM_IT_CC1);
	TIM_ITConfig(TIM10, TIM_IT_CC1, ENABLE);

	/* Enable the TIM1 global Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_TIM10_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 14;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_Cmd(TIM10, ENABLE);
}

/**
 * =========================
 * @function: TIM10_ICConfig
 */
void TIM1_UPT_TIM10_IRQHandler() {
	static uint16_t tim10_pre_capture_val = 0;
	static uint16_t tim10_cur_capture_val = 0;
	uint16_t tim10_ret_capture_val = 0;
	double tim10_period_val = 0;
	double tim10_freq_ret = 0;

	if(TIM_GetITStatus(TIM10, TIM_IT_CC1) == SET) {
		TIM_ClearFlag(TIM10, TIM_IT_CC1);
		TIM_ClearITPendingBit(TIM10, TIM_IT_CC1);
		/* TO DO */
		tim10_pre_capture_val = tim10_cur_capture_val;
		tim10_cur_capture_val = TIM_GetCapture1(TIM10);

		if (tim10_cur_capture_val > tim10_pre_capture_val) {
			tim10_ret_capture_val = tim10_cur_capture_val - tim10_pre_capture_val;
		}
		else if (tim10_cur_capture_val < tim10_pre_capture_val) {
			tim10_ret_capture_val = ((TIM10_IC_PERIOD - tim10_pre_capture_val) + tim10_cur_capture_val);
		}
		else {
			tim10_ret_capture_val = 0;
		}

		/* Frequency computation */
		if (tim10_pre_capture_val != 0) {
			tim10_period_val = (double)tim10_ret_capture_val * ((double)1 / TIM10_IC_COUNTER_FREQ);
			tim10_freq_ret = (double)1 / tim10_period_val;

			__DISPLAY__("Period = %.3f(s)", tim10_period_val);
			__DISPLAY__("Freq = %0.3f(Hz)", tim10_freq_ret);
		}
	}
}
